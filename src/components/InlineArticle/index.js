import React from 'react';
import Link from 'next/link';
import styled from 'styled-components';
import { MdRemoveRedEye, MdComment } from 'react-icons/md';
import { media } from 'styles';

const InlineArticle = ({
  title,
  image,
  label,
  description,
  published_at,
  category,
  count,
  id,
  slug,
}) => (
  <Content>
    <Link href="/article/[...slug]" as={`/article/${id}/${slug}`}>
      <Image style={{ backgroundImage: `url(${image})` }} />
    </Link>
    <MainContent>
      <Header>
        <Label color={category.color}>{category.title}</Label>
        <span>{published_at}</span>
      </Header>
      <div>
        <h3>
          <Link href="/article/[...slug]" as={`/article/${id}/${slug}`}>
            <a>{title}</a>
          </Link>
        </h3>
        <p>
          {description.length >= 260
            ? `${description.slice(0, 260)}...`
            : description}
        </p>
      </div>
      <Stats>
        <StatItem>
          <MdRemoveRedEye />
          <span>{count.views}</span>
        </StatItem>
        <StatItem>
          <MdComment />
          <span>{count.comments}</span>
        </StatItem>
      </Stats>
    </MainContent>
  </Content>
);

export default InlineArticle;

const Content = styled.div`
  display: grid;
  grid-template-columns: 240px auto;
  grid-gap: 0 30px;
  align-items: flex-start;
  margin-bottom: 60px;
  ${media.phone`
    grid-template-columns: 1fr;
    grid-gap: 24px 0;
  `}
`;

const MainContent = styled.div`
  border-right: 3px ${({ theme }) => theme.red} solid;
  padding-right: 30px;
  h3 {
    font-size: 18px;
    line-height: 27px;
    font-weight: 500;
  }
  p {
    font-size: 14px;
  }

  ${media.phone`
    border-right: none;
    p {
      font-size: 16px;
    }
  `}
`;

const Image = styled.a`
  min-height: 160px;
  display: block;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center center;
  ${media.phone`
    min-height: 220px;
  `}
`;

const Label = styled.div`
  background: ${({ theme, color }) => color || theme.red};
  font-size: 13px;
  padding: 3px 10px;
  font-weight: 600;
  color: #fff;
  border-radius: 2px;
  height: 26px;
  display: flex;
  align-items: center;
`;

const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 15px;
  & > span {
    font-size: 14px;
    font-weight: 400;
    margin-left: 20px;
    color: rgba(0, 0, 0, 0.4);
  }
`;

const Stats = styled.div`
  display: grid;
  justify-content: start;
  grid-auto-flow: column;
  grid-gap: 0 11px;
  margin-top: 15px;
  width: 100%;
`;

const StatItem = styled.div`
  display: flex;
  align-items: center;
  color: #777777;
  span {
    font-size: 15px;
    margin-left: 5px;
  }
`;
