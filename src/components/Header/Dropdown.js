import React from 'react';
import { connect } from 'react-redux';
import { effects } from 'redux/ducks/auth.duck';
import styled from 'styled-components';
import { FiUser, FiEdit2, FiUserX } from 'react-icons/fi';
import { ResetDefaultButton } from 'styles';

function Dropdown({ profile, openModal, authLogout }) {
  return profile ? (
    <SignInBtn>
      <ProfileInfo>
        <span>
          {profile.name} {profile.last_name}
        </span>
        <img src={profile.image} alt="" />
      </ProfileInfo>
      <DropdownItems>
        <DropdownItem onClick={() => openModal({ type: 'editProfile' })}>
          <FiEdit2 size={18} />
          Редактировать
        </DropdownItem>
        <DropdownItem onClick={authLogout}>
          <FiUserX size={18} />
          Выйти
        </DropdownItem>
      </DropdownItems>
    </SignInBtn>
  ) : (
    <SignInBtn onClick={() => openModal({ type: 'signIn' })}>
      <FiUser size={18} color="#f13d37" /> Вход
    </SignInBtn>
  );
}

export default connect(null, { ...effects })(Dropdown);

const DropdownItems = styled.div`
  position: absolute;
  visibility: hidden;
  width: 160px;
  top: 100%;
  right: 0px;
  background: #fff;
  z-index: 800;
  border: 1px solid rgba(0, 0, 0, 0.15);
  padding: 4px 0;
  border-radius: 4px;
  box-shadow: 0 1px 5px rgba(0, 0, 0, 0.15);
  opacity: 0;
  filter: alpha(opacity=0);
  transition: opacity 100ms linear, top 100ms linear, visibility 100ms linear;

  &:before {
    position: absolute;
    pointer-events: none;
    border: solid transparent;
    content: '';
    height: 0;
    width: 0;
    bottom: 100%;
    right: 42px;
    border-width: 6px;
    margin: 0 -6px;
    border-bottom-color: rgba(0, 0, 0, 0.15);
  }
`;

const DropdownItem = styled.div`
  display: flex;
  align-items: center;
  white-space: nowrap;
  position: relative;
  height: 45px;
  padding: 0 15px;
  color: #000;
  transition: all 0.1s ease;
  svg {
    margin-right: 8px;
  }
  &:hover {
    background: rgb(241, 61, 55, 0.1);
  }
`;

const SignInBtn = styled.button`
  ${ResetDefaultButton}
  font-size: 14px;
  font-weight: 500;
  cursor: pointer;
  display: flex;
  align-items: center;
  position: relative;
  padding: 0 8px;
  transition: all 0.2s ease;
  &:hover {
    background: rgba(0, 0, 0, 0.043);
    ${DropdownItems} {
      opacity: 1;
      visibility: visible;
    }
  }
  svg {
    margin-right: 5px;
  }
`;

const ProfileInfo = styled.div`
  display: grid;
  align-items: center;
  grid-template-columns: auto 32px;
  justify-content: flex-end;
  grid-gap: 0 6px;
  img {
    object-fit: cover;
    width: 32px;
    height: 32px;
    border-radius: 50%;
  }
`;
