import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Link from 'next/link';
import { Container, media, ResetDefaultButton } from 'styles';
import { IoIosClose } from 'react-icons/io';
import SmallArticle from 'components/SmallArticle';
import SocialIcons from 'components/Social';

function Navigation({ isActive, categories, socials, populars }) {
  return (
    <NavContainer className={`${isActive ? 'active' : ''} main-navigation`}>
      <BackgroundClose />
      <Content>
        <ButtonClose>
          <button type="button">
            Закрыть <IoIosClose size={38} />
          </button>
        </ButtonClose>
        <MainContainer>
          <Row>
            <Col>
              <Title>Рубрики</Title>
              <Rubki>
                {categories.length > 0 &&
                  categories.map(({ title, id }) => (
                    <Link
                      href={{
                        pathname: '/search',
                        query: {
                          category_id: id,
                        },
                      }}
                      key={id}
                    >
                      <a>{title}</a>
                    </Link>
                  ))}
              </Rubki>
            </Col>

            <Col>
              <Title>Популярные</Title>
              <TopCountList>
                {populars &&
                  populars.slice(0, 2).map((popular, index) => {
                    const { id } = popular;

                    return (
                      <TopCountItem key={id}>
                        <TopCount>{`0${index + 1}`}</TopCount>
                        <SmallArticle {...popular} />
                      </TopCountItem>
                    );
                  })}
              </TopCountList>
              <Title>Социальные сети</Title>
              <Social>
                {socials.length > 0 &&
                  socials.map(({ id, ...social }) => (
                    <SocialIcons key={id} {...social} />
                  ))}
              </Social>
            </Col>
          </Row>
        </MainContainer>
      </Content>
    </NavContainer>
  );
}

Navigation.propTypes = {
  isActive: PropTypes.bool.isRequired,
  categories: PropTypes.instanceOf(Array).isRequired,
  socials: PropTypes.instanceOf(Array).isRequired,
};

export default Navigation;

const NavContainer = styled.div`
  position: absolute;
  top: 80%;
  left: 0;
  width: 100%;
  height: 100%;
  opacity: 0;
  visibility: hidden;
  overflow: inherit;
  box-shadow: 2px 2px 10px rgba(141, 150, 178, 0.3);
  transition: all 0.2s, top 0.3s cubic-bezier(0.175, 0.885, 0.135, 1.425);
  z-index: 1000;

  &.active {
    top: 100%;
    opacity: 1;
    visibility: visible;
  }

  ${media.phone`
    position: fixed;
    top: -40px;

    &.active {
      top: 0;
      opacity: 1;
      visibility: visible;
      overflow-y: auto;
    }
  `}
`;

const Content = styled.div`
  background: #c62828;
  padding: 60px 0;
  position: relative;
  z-index: 1;

  a {
    color: #fff;
  }

  ${media.phone`
    padding: 20px 0 60px;
  `}
`;

const BackgroundClose = styled.div`
  display: none;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.8);
  z-index: -1;
  ${media.phone`
    display: block;
  `}
`;

const MainContainer = styled.div`
  ${Container}
  display: flex;
  flex-direction: column;
`;

const Title = styled.h3`
  color: rgba(255, 255, 255, 0.6);
  padding-right: 30px;
  padding-bottom: 10px;
  border-bottom: 3px #fff solid;
  display: inline-block;
  margin-bottom: 28px;
  font-size: 16px;
`;

const Row = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: 0.7fr 0.3fr;
  grid-gap: 0 50px;
  ${media.desktop`
    grid-template-columns: 1fr;
    grid-gap: 24px 0;
  `}
`;

const Col = styled.div``;

const Rubki = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-gap: 14px;
  a {
    font-size: 15px;
    font-weight: 500;
  }

  ${media.phone`
    grid-template-columns: 1fr 1fr;
  `}
`;

const Social = styled.div`
  display: grid;
  grid-auto-flow: column;
  justify-content: start;
  grid-gap: 0 25px;
  a {
    width: 48px;
    height: 48px;
    background: rgba(255, 255, 255, 0.6);
    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: center;
    color: rgba(0, 0, 0, 0.8);
  }
`;

const ButtonClose = styled.div`
  display: none;
  align-items: center;
  justify-content: end;
  padding: 0 15px;
  margin-bottom: 25px;

  button {
    ${ResetDefaultButton}
    color: #fff;
  }
  ${media.phone`
    display: grid;
  `}
`;

const TopCountList = styled.div`
  display: grid;
  grid-gap: 22px 0;
  margin-bottom: 20px;
  span,
  svg {
    color: #fff;
  }
`;

const TopCountItem = styled.div`
  display: grid;
  grid-template-columns: auto 1fr;
  grid-gap: 0 16px;
`;

const TopCount = styled.div`
  font-size: 34px;
  color: rgba(255, 255, 255, 0.6);
`;
