import React, { useLayoutEffect, useRef, useState } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { connect } from 'react-redux';
import { actions } from 'redux/ducks/modals.duck';
import styled from 'styled-components';
import { Container, ResetDefaultButton, media } from 'styles';
import { IoIosArrowDown, IoIosSearch, IoMdClose } from 'react-icons/io';
import { FaBullhorn } from 'react-icons/fa';
import { Field, Formik, Form } from 'formik';
import logo from 'assets/images/logo.png';
import Navigation from './Navigation';
import Dropdown from './Dropdown';

function Header({
  categories,
  socials,
  promotions,
  loadingPromotions,
  settings,
  openModal,
  profile,
  populars,
}) {
  const [isActive, setActive] = useState(false);
  const [isSearch, setSearch] = useState(false);
  const _input = useRef();
  const router = useRouter();

  useLayoutEffect(() => {
    const handleHideDropdown = event => {
      if (
        (isActive && event.target.classList.contains('drop-menu')) ||
        (isActive && event.target.closest('.drop-menu'))
      ) {
        return;
      }

      setActive(false);
    };

    document.addEventListener('click', handleHideDropdown);

    return () => {
      document.removeEventListener('click', handleHideDropdown);
    };
  }, [isActive]);

  useLayoutEffect(() => {
    document.addEventListener('keydown', event => {
      if (event.key === 'Escape') {
        setSearch(false);
      }
    });
  }, []);

  useLayoutEffect(() => {
    if (isSearch) _input.current.focus();
  }, [isSearch]);

  function handleSearch() {
    setSearch(!isSearch);
  }

  function handleDropdown() {
    setActive(!isActive);
  }
  // {/* {loadingPromotions && <LoadingDefault />} */}
  // className="header"
  return (
    <Content>
      <HeaderWrap>
        <MainContainer>
          <HeaderBanner>
            <div>
              <FaBullhorn size={20} />
            </div>
          </HeaderBanner>
          <Dropdown profile={profile} openModal={openModal} />
        </MainContainer>
      </HeaderWrap>

      <MainHeader>
        <MainContainer>
          <div>
            <Link href="/">
              <Logo to="/">
                <img
                  src={logo}
                  alt="Официальный сайт медиа-холдинга Красный Бумер"
                />
              </Logo>
            </Link>
            <LogoText>
              <h3>
                Главный портал <br /> Красного Сулина
              </h3>
              <p style={{ fontWeight: 400 }}>
                Официальный сайт медиа-холдинга "Красный Бумер".
              </p>
            </LogoText>
          </div>
          {settings && settings.appBanner && (
            <LogoPromotion
              style={{ backgroundImage: `url(${settings.appBanner.value})` }}
            />
          )}
        </MainContainer>
      </MainHeader>

      {/* <MainContainer>
        {promotions && promotions.main ? (
          <a href={promotions.main.link}>
            <img src={promotions.main.image} alt="" />{' '}
          </a>
        ) : (
          <Advertising>
            <h4>Здесь может быть ваша реклама</h4>
          </Advertising>
        )}
      </MainContainer> */}

      <Nav onClick={e => e.stopPropagation()}>
        <MainContainer>
          <NavList>
            {categories.length > 0 &&
              categories.slice(0, 5).map(({ id, title }) => (
                <Link
                  key={id}
                  href={{
                    pathname: '/search',
                    query: {
                      category_id: id,
                    },
                  }}
                >
                  <NavItem>{title}</NavItem>
                </Link>
              ))}
            <button
              type="button"
              className="drop-menu"
              onClick={handleDropdown}
            >
              Все рубрики
              <IoIosArrowDown size={20} />
            </button>
          </NavList>
          <Search>
            <Formik
              initialValues={{ search: '' }}
              onSubmit={({ search }) => {
                router.push({
                  pathname: '/search',
                  query: {
                    search,
                  },
                });

                handleSearch();
              }}
            >
              {({ resetForm }) => (
                <Form>
                  <StyledForm isSearch={isSearch}>
                    <Field
                      name="search"
                      innerRef={_input}
                      placeholder="Поиск"
                      onBlur={() => {
                        setSearch(false);
                        resetForm();
                      }}
                    />
                    <SearchBtn type="button" onClick={handleSearch}>
                      {isSearch ? (
                        <IoMdClose size={30} />
                      ) : (
                        <IoIosSearch size={30} />
                      )}
                    </SearchBtn>
                  </StyledForm>
                </Form>
              )}
            </Formik>
          </Search>
        </MainContainer>
        <Navigation
          isActive={isActive}
          categories={categories}
          socials={socials}
          populars={populars}
        />
      </Nav>
    </Content>
  );
}

export default connect(null, { ...actions })(Header);

const Content = styled.div`
  position: relative;
`;

const MainContainer = styled.div`
  ${Container}
`;

const Logo = styled.a`
  display: block;
  /* width: 100px; */
`;

const HeaderWrap = styled.div`
  border-bottom: 1px solid #e8eaf0;
  margin-bottom: 20px;
  & > div {
    min-height: 38px;
    display: grid;
    justify-content: space-between;
    grid-auto-flow: column;
    ${media.phone`
      justify-content: end;
    `}
  }
`;

const HeaderBanner = styled.div`
  display: grid;
  grid-auto-flow: column;
  align-items: center;
  grid-gap: 16px;
  height: 38px;
  .swiper-container {
    max-height: 38px;
  }
  .swiper-slide {
    display: flex;
    align-items: center;
  }
  & > div {
    &:first-child {
      background: #f13d37;
      width: 110px;
      padding-left: 15px;
      display: flex;
      align-items: center;
      align-self: stretch;
      svg {
        color: #fff;
      }
    }
  }

  ${media.phone`
    display: none;
  `}
`;

const MainHeader = styled.div`
  & > div {
    position: relative;
    min-height: 56px;
    display: grid;
    grid-gap: 0 36px;
    align-items: center;
    /* grid-auto-flow: column; */
    grid-template-columns: 1fr 1fr;
    ${media.phone`
      display: block;
    `}
    & > div {
      display: grid;
      align-items: start;
      grid-auto-flow: column;
      grid-gap: 0 24px;
      &:first-child {
        grid-template-columns: 152px auto;
        ${media.tablet`
          align-items: center;
          justify-content: start;
          grid-template-columns: 90px auto;
          grid-gap: 0 12px;
        `}
      }
    }
  }

  ${media.tablet`
    & > div {
      grid-template-columns: 330px auto;
      align-items: start;
    }
  `}

  ${media.phone`
    grid-template-columns: 1fr;
  `}
`;

const Search = styled.div`
  justify-self: end;
  ${media.phone`
    display: none;
  `}
`;

const SearchBtn = styled.button`
  ${ResetDefaultButton}
  display: flex;
  justify-content: center;
  align-items: center;
  transition: opacity 0.2s ease;
  position: relative;
  z-index: 10;
  &:hover {
    opacity: 0.8;
    color: #000 !important;
  }
`;

const NavList = styled.div`
  display: grid;
  position: relative;
  grid-auto-flow: column;
  align-items: center;
  grid-gap: 0 30px;
  ${media.desktop`
    a {
      display: none;
      white-space: nowrap; 
      overflow: hidden;
      text-overflow: ellipsis;
      &:first-child, &:nth-child(2), &:nth-child(3) {
        display: block;
      }
    }
  `}

  ${media.phone`
    a, button {
      font-size: 18px;
    }
    a {
      white-space: nowrap; 
      overflow: hidden;
      text-overflow: ellipsis;
    }
    a {
      &:nth-child(2), &:nth-child(3), &:nth-child(4) {
        display: none;
      }
    }
  `}
`;

const Nav = styled.div`
  position: relative;
  margin-top: 40px;
  border-bottom: 1px solid #e8eaf0;
  padding-bottom: 10px;

  & > div {
    &:first-child {
      display: grid;
      position: relative;
      align-items: center;
      grid-gap: 0 30px;
      justify-content: space-between;
      grid-template-columns: auto 56px;
      ${media.phone`
        grid-template-columns: 100%;
      `}
    }
  }

  button {
    display: flex;
    align-items: center;
    font-size: 15px;
    border: none;
    background: none;
    font-weight: 500;
    cursor: pointer;
    transition: all 0.2s ease;
    position: relative;

    & > svg {
      margin-left: 4px;
    }

    &:hover {
      color: ${({ theme }) => theme.red};
      opacity: 0.8;
    }
    &:focus {
      outline: none;
    }

    ${media.phone`
      font-size: 18px;
    `}
  }
`;

const NavItem = styled.a`
  font-size: 15px;
  font-weight: 500;
  &:hover {
    color: ${({ theme }) => theme.red};
  }
`;

const Advertising = styled.div`
  padding: 0;
  margin-top: 25px;
  background-color: ${({ theme }) => theme.red};
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  height: 160px;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0 20px;
  h4 {
    color: #fff;
    font-size: 28px;
    margin: 0;
    line-height: 36px;
  }
  a {
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    z-index: 1;
  }
  h4 {
  }
`;

const LogoPromotion = styled.div`
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  height: 100px;
  position: relative;
  a {
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    z-index: 1;
  }

  ${media.phone`
    display: none !important;
  `}
`;

const StyledForm = styled.div`
  & > input {
    z-index: 1;
    max-width: 990px;
    width: 100%;
    height: 56px;
    bottom: -12px;
    right: 0;
    background: #ecebeb;
    border: none;
    font-size: 24px;
    font-weight: 400;
    padding-left: 25px;
    padding-right: 70px;
    position: absolute;
    display: ${({ isSearch }) => (isSearch ? 'block' : 'none')};
    &:focus {
      outline: none;
    }
  }
`;

const LogoText = styled.div`
  h3 {
    font-size: 30px;
    line-height: 1.2;
    font-weight: 400;
    margin: 0 0 6px;
  }
  p {
    margin: 0;
    font-size: 14px;
  }

  ${media.tablet`
    h3 {
      font-size: 21px;
    }
  `}

  ${media.phone`
    p {
      display: none;
    }
  `}
`;
