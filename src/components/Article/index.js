import React from 'react';
import Link from 'next/link';
import styled from 'styled-components';
import { MdRemoveRedEye, MdComment } from 'react-icons/md';
import { media } from 'styles';

const size = {
  block: 384,
  image: 352,
  title: 64,
  description: 96,
};

function updateDesc(desc) {
  const replaceDesc = desc
    .split(' ')
    .map(e => e.replace('\u00A0', ' '))
    .join(' ')
    .trim();

  return replaceDesc.length > 140
    ? `${replaceDesc.slice(0, 140)}...`
    : replaceDesc;
}

const Article = ({
  title,
  image,
  description,
  published_at,
  category,
  count,
  id,
  slug,
}) => (
  <Content>
    <Link href="/article/[...slug]" as={`/article/${id}/${slug}`}>
      <Thumbnail>
        <Image
          style={{
            backgroundImage: `url(${image})`,
          }}
        />
        <Date>{published_at}</Date>
      </Thumbnail>
    </Link>
    <MainContent>
      <Label color={category.color}>{category.title}</Label>
      <h3>
        <Link href="/article/[...slug]" as={`/article/${id}/${slug}`}>
          <a>{title}</a>
        </Link>
      </h3>
      <p>{updateDesc(description)}</p>
    </MainContent>
    <Stats>
      <StatItem>
        <MdRemoveRedEye />
        <span>{count.views}</span>
      </StatItem>
      <StatItem>
        <MdComment />
        <span>{count.comments}</span>
      </StatItem>
    </Stats>
  </Content>
);

export default Article;

const Image = styled.div`
  width: 100%;
  min-height: ${size.image}px;
  max-height: ${size.image}px;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  transition: all 0.3s ease;
  ${media.desktop`
    min-height: 280px;
    max-height: 280px;
  `}
`;

const Label = styled.div`
  background: ${({ theme, color }) => color || theme.red};
  font-size: 13px;
  padding: 3px 10px;
  font-weight: 600;
  color: #fff;
  border-radius: 2px;
  height: 26px;
  display: inline-flex;
  align-items: center;
  position: absolute;
  top: -35px;
`;

const MainContent = styled.div`
  background: #fff;
  position: absolute;
  width: 100%;
  z-index: 1;
  left: 0;
  bottom: 0;
  padding: 10px;
  transform: translateY(116px);
  margin-bottom: 44px;
  transition: all 0.3s ease;
  h3 {
    font-size: 20px;
    line-height: 24px;
    font-weight: 500;
    /* min-height: ${size.title}px; */
    /* max-height: ${size.title}px; */
    a {
      width: 100%;
    }
    ${media.desktop`
      margin-bottom: 0;
    `}
  }

  p {
    min-height: ${size.description}px;
    /* max-height: ${size.description}px; */
    opacity: 1;
    visibility: visible;
    overflow: hidden;
    color: #666666;
    font-weight: 400;
    margin: 0;
    transition: all 0.3s ease;
  }

  ${media.desktop`
    position: relative;
    margin-bottom: 0;
    h3 {
      min-height: inherit;
      max-height: inherit;
      margin-bottom: 20px;
    }
    p {
      min-height: inherit;
      max-height: inherit;
    }
    transform: translateY(0);
  `}
`;

const Stats = styled.div`
  display: grid;
  justify-content: start;
  grid-auto-flow: column;
  grid-gap: 0 11px;
  margin-top: 15px;
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  padding: 14px 10px;
  z-index: 2;
  background: #fff;
  ${media.desktop`
    position: relative;
    margin-top: 0;
  `}
`;

const StatItem = styled.div`
  display: flex;
  align-items: center;
  color: #777777;
  span {
    font-size: 14px;
    margin-left: 5px;
  }
`;

const Date = styled.div`
  background: rgba(0, 0, 0, 0.6);
  color: #fff;
  font-size: 14px;
  font-weight: normal;
  padding: 6px 8px;
  position: absolute;
  top: 10px;
  right: 10px;
`;

const Thumbnail = styled.a`
  background: #000000;
  display: block;
  &:hover {
    opacity: 1;
  }
`;

const Content = styled.div`
  position: relative;
  overflow: hidden;
  height: ${size.block}px;
  box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.15);
  background: rgba(0, 0, 0, 0.2);
  &:hover {
    ${MainContent} {
      transform: translateY(0);
    }
    ${Image} {
      transform: scale(1.2);
      opacity: 0.4;
      ${media.desktop`
        transform: scale(1.1);
        opacity: 1;
      `}
    }
  }

  ${media.desktop`
    height: auto;
    display: grid;
  `}
`;
