import React, { useEffect, useRef } from 'react';
import {
  disableBodyScroll,
  enableBodyScroll,
  clearAllBodyScrollLocks,
} from 'body-scroll-lock';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { actions, selectors } from 'redux/ducks/modals.duck';
import { media } from 'styles';
import MainPortal from './MainPortal';
import RenderModals from './renderModals';

function ModalContent({ isOpen, ...props }) {
  const modal = useRef(null);

  useEffect(() => {
    const targetElement = modal.current;
    const { body } = document;

    if (isOpen) {
      if (navigator.appVersion.indexOf('Win') !== -1) {
        body.setAttribute('style', 'overflow: hidden; padding-right: 17px');
      } else {
        disableBodyScroll(targetElement);
      }
    } else {
      enableBodyScroll(targetElement);
      body.removeAttribute('style');
    }

    return () => {
      clearAllBodyScrollLocks();
    };
  }, [isOpen]);

  return (
    <MainPortal {...props}>
      <Container ref={modal} className={isOpen ? 'isOpen' : ''}>
        <RenderModals {...props} />
      </Container>
    </MainPortal>
  );
}

export default connect(
  state => ({
    isOpen: selectors.isOpen(state),
    args: selectors.getArgs(state),
    type: selectors.getType(state),
  }),
  { ...actions },
)(ModalContent);

const Container = styled.div`
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  position: fixed;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.6);
  -webkit-overflow-scrolling: touch;
  z-index: 1000;
  opacity: 0;
  overflow: hidden;
  visibility: hidden;
  transition: all 0.2s ease;
  display: grid;
  justify-content: center;
  align-items: center;
  grid-template-columns: 1fr;
  padding-bottom: 40px;
  padding-top: 40px;
  &.isOpen {
    opacity: 1;
    visibility: visible;
    overflow-x: hidden;
    overflow-y: auto;
  }

  ${media.phone`
    padding: 0 10px;
  `}
`;
