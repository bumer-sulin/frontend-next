import React from 'react';
import ModalContainer from './ModalContainer';
import ImageGallery from './render/ImageGallery';
import Signin from './render/Signin';
import EditProfile from './render/EditProfile';
import Registration from './render/Registation';
import ResetPassword from './render/ResetPassword';
import GetPassword from './render/GetPassword';

const RenderModal = ({ type, args, ...props }) => {
  switch (type) {
    case 'gallery':
      return (
        <ModalContainer {...props} center>
          <ImageGallery {...args} />
        </ModalContainer>
      );
    case 'signIn':
      return (
        <ModalContainer maxWidth="420px" {...props} center>
          <Signin {...args} />
        </ModalContainer>
      );
    case 'editProfile':
      return (
        <ModalContainer maxWidth="420px" {...props} center>
          <EditProfile {...args} />
        </ModalContainer>
      );
    case 'registration':
      return (
        <ModalContainer maxWidth="420px" {...props} center>
          <Registration {...args} />
        </ModalContainer>
      );
    case 'resetPassword':
      return (
        <ModalContainer maxWidth="420px" {...props} center>
          <ResetPassword {...args} />
        </ModalContainer>
      );
    case 'getPassword':
      return (
        <ModalContainer maxWidth="420px" {...props} center>
          <GetPassword {...args} />
        </ModalContainer>
      );
    default:
      return null;
  }
};

export default RenderModal;
