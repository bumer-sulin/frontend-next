import React, { useEffect, useState } from 'react';
import axios from 'axios';
import VK, { Auth } from 'react-vk';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { selectors } from 'redux/ducks/main.duck';
import { effects } from 'redux/ducks/auth.duck';
import { actions } from 'redux/ducks/user.duck';
import { media } from 'styles';
import { Formik, Field, Form } from 'formik';
import Input from 'components/UI/Input';
import SocialIcons from 'components/Social';

const Sigin = ({
  closeModal,
  openModal,
  authLogin,
  socials,
  fetchProfileRequest,
}) => {
  const [loading, setLoading] = useState(true);

  function handleLoading() {
    return setTimeout(() => setLoading(false), 400);
  }

  useEffect(() => {
    handleLoading();

    return () => clearTimeout(handleLoading);
  }, []);

  return (
    <Content>
      <h2>Авторизация</h2>
      <Formik
        validate={values => {
          const errors = {};

          if (!values.email) {
            errors.email = 'Обязательное поле';
          } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
          ) {
            errors.email = 'Неправильно введен email';
          }

          if (values.password.length < 3) {
            errors.password = 'Пароль должен быть больше 3 символов';
          }

          return errors;
        }}
        initialValues={{ email: '', password: '' }}
        onSubmit={(values, { setErrors, setSubmitting }) =>
          authLogin(values, setErrors, setSubmitting)
        }
      >
        {({ errors, isSubmitting, ...props }) => (
          <StyledForm>
            <Field
              type="email"
              placeholder="Email"
              name="email"
              component={Input}
            />
            <div>
              <Field
                type="password"
                placeholder="Пароль"
                name="password"
                component={Input}
              />
              <CustomLink onClick={() => openModal({ type: 'getPassword' })}>
                Восстановить пароль
              </CustomLink>
            </div>
            <div>
              <Button
                type="submit"
                disabled={!!Object.keys(errors).length || isSubmitting}
              >
                Войти
              </Button>
              <CustomLink
                style={{ textAlign: 'right', marginTop: '14px' }}
                onClick={() => openModal({ type: 'registration' })}
              >
                Зарегистрироваться
              </CustomLink>
            </div>

            <SocialItems>
              <h4>Войти через соц сети</h4>
              <div>
                {[
                  {
                    type: { title: 'google' },
                    id: 2,
                    link: 'https://bumer-media.ru/auth/google/redirect',
                  },

                  {
                    type: { title: 'yandex' },
                    id: 3,
                    link: 'https://bumer-media.ru/auth/yandex/redirect',
                  },

                  {
                    type: { title: 'facebook' },
                    id: 4,
                    link: 'https://bumer-media.ru/auth/facebook/redirect',
                  },
                ].map(({ id, ...social }) => (
                  <SocialIcons key={id} {...social} />
                ))}
              </div>
            </SocialItems>
            {!loading && (
              <VK apiId={6909244}>
                <Auth
                  options={{
                    onAuth: user => {
                      return axios
                        .get('https://bumer-media.ru/auth/vkontakte/callback', {
                          params: { ...user, api: 1 },
                        })
                        .then(res => {
                          fetchProfileRequest(res.data.hash);
                          closeModal();
                        })
                        .catch(error => {});
                    },
                    // authUrl: 'https://bumer-media.ru/auth/vkontakte/callback',
                  }}
                />
              </VK>
            )}
          </StyledForm>
        )}
      </Formik>
    </Content>
  );
};

export default connect(
  state => ({
    socials: selectors.getSocials(state),
  }),
  { ...effects, ...actions },
)(Sigin);

const Content = styled.div`
  background: #fff;
  padding: 20px 20px 40px;
  max-width: 420px;
  width: 100%;
  h2 {
    text-align: center;
    margin-bottom: 40px;
    font-weight: 500;
  }

  ${media.phone`
    /* width: 300px; */
  `}
`;

const StyledForm = styled(Form)`
  display: grid;
  grid-gap: 24px 0;

  & > div {
    &:last-child {
      justify-self: center;
    }
  }
`;

const Button = styled.button`
  width: 100%;
  cursor: pointer;
  border: none;
  padding: 0 20px;
  height: 52px;
  color: #fff;
  font-size: 16px;
  background: ${({ theme }) => theme.red};
`;

const CustomLink = styled.div`
  color: #0c729c;
  font-size: 14px;
  margin-top: 12px;
  text-align: left;
  cursor: pointer;
  transition: all 0.2s ease;
  &:hover {
    opacity: 0.8;
  }
`;

const SocialItems = styled.div`
  h4 {
    text-align: center;
    font-weight: 500;
  }

  & > div {
    display: grid;
    align-items: center;
    justify-content: center;
    grid-auto-flow: column;
    grid-gap: 0 15px;
    a {
      border-radius: 0;
    }
  }
`;
