import React, { useEffect, useState } from 'react';
import VK, { Auth } from 'react-vk';
import axios from 'axios';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { effects } from 'redux/ducks/auth.duck';
import { actions } from 'redux/ducks/user.duck';
import { Formik, Field, Form } from 'formik';
import Input from 'components/UI/Input';
import SocialIcons from 'components/Social';

function Registration({
  closeModal,
  openModal,
  authSignup,
  fetchProfileRequest,
}) {
  const [loading, setLoading] = useState(true);

  function handleLoading() {
    return setTimeout(() => setLoading(false), 400);
  }

  useEffect(() => {
    handleLoading();

    return () => clearTimeout(handleLoading);
  }, []);

  return (
    <Content>
      <h2>Регистрация</h2>
      <Formik
        validate={values => {
          const errors = {};

          if (!values.email) {
            errors.email = 'Обязательное поле';
          } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
          ) {
            errors.email = 'Неправильно введен email';
          }

          if (values.first_name < 3) {
            errors.name = 'Мало символов';
          }

          if (values.last_name < 3) {
            errors.name = 'Мало символов';
          }

          if (values.password.length < 3) {
            errors.password = 'Пароль должен быть больше 3 символов';
          }

          if (values.password_confirmation !== values.password) {
            errors.password_confirmation = 'Пароль не совпадает';
          }

          return errors;
        }}
        initialValues={{
          email: '',
          first_name: '',
          last_name: '',
          password: '',
          password_confirmation: '',
        }}
        onSubmit={(values, { setErrors, setSubmitting }) => {
          authSignup(values, setErrors, setSubmitting);
        }}
      >
        {({ errors, isSubmitting, ...props }) => (
          <StyledForm>
            <Field
              type="email"
              placeholder="Email"
              name="email"
              component={Input}
            />
            <Field placeholder="Имя" name="first_name" component={Input} />
            <Field placeholder="Фамилия" name="last_name" component={Input} />
            <Field
              type="password"
              placeholder="Придумайте пароль"
              name="password"
              component={Input}
            />
            <Field
              type="password"
              placeholder="Повторите пароль"
              name="password_confirmation"
              component={Input}
            />
            <div>
              <Button
                type="submit"
                disabled={!!Object.keys(errors).length || isSubmitting}
              >
                Зарегистрироваться
              </Button>
              <CustomLink
                style={{ textAlign: 'right', marginTop: '14px' }}
                onClick={() => openModal({ type: 'signIn' })}
              >
                Авторизоваться
              </CustomLink>
            </div>

            <SocialItems>
              <h4>Войти через соц сети</h4>
              <div>
                {[
                  {
                    type: { title: 'google' },
                    id: 2,
                    link: 'https://bumer-media.ru/auth/google/redirect',
                  },

                  {
                    type: { title: 'yandex' },
                    id: 3,
                    link: 'https://bumer-media.ru/auth/yandex/redirect',
                  },

                  {
                    type: { title: 'facebook' },
                    id: 4,
                    link: 'https://bumer-media.ru/auth/facebook/redirect',
                  },
                ].map(({ id, ...social }) => (
                  <SocialIcons key={id} {...social} />
                ))}
              </div>
            </SocialItems>

            {!loading && (
              <VK apiId={6909244}>
                <Auth
                  options={{
                    onAuth: user => {
                      return axios
                        .get('https://bumer-media.ru/auth/vkontakte/callback', {
                          params: { ...user, api: 1 },
                        })
                        .then(res => {
                          fetchProfileRequest(res.data.hash);
                          closeModal();
                        })
                        .catch(error => {});
                    },
                    // authUrl: 'https://bumer-media.ru/auth/vkontakte/callback',
                  }}
                />
              </VK>
            )}
          </StyledForm>
        )}
      </Formik>
    </Content>
  );
}

export default connect(null, { ...effects, ...actions })(Registration);

const Content = styled.div`
  background: #fff;
  padding: 20px 20px 40px;
  max-width: 420px;
  h2 {
    text-align: center;
    margin-bottom: 40px;
  }
`;

const StyledForm = styled(Form)`
  display: grid;
  grid-gap: 24px 0;

  & > div {
    &:last-child {
      justify-self: center;
    }
  }
`;

const Button = styled.button`
  width: 100%;
  cursor: pointer;
  border: none;
  padding: 0 20px;
  height: 52px;
  color: #fff;
  font-size: 16px;
  background: ${({ theme }) => theme.red};
`;

const CustomLink = styled.div`
  color: #0c729c;
  font-size: 14px;
  margin-top: 12px;
  text-align: left;
  cursor: pointer;
  transition: all 0.2s ease;
  &:hover {
    opacity: 0.8;
  }
`;

const SocialItems = styled.div`
  h4 {
    text-align: center;
    font-weight: 500;
  }

  & > div {
    display: grid;
    align-items: center;
    justify-content: center;
    grid-auto-flow: column;
    grid-gap: 0 15px;
    a {
      border-radius: 0;
    }
  }
`;
