import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { selectors } from 'redux/ducks/main.duck';
import { effects } from 'redux/ducks/auth.duck';
import { media } from 'styles';
import { Formik, Field, Form } from 'formik';
import Input from 'components/UI/Input';
import { MdKeyboardBackspace } from 'react-icons/md';

function GetPassword({
  closeModal,
  openModal,
  authLogin,
  socials,
  getPassword,
}) {
  return (
    <Content>
      <h2>Восстановить пароль</h2>
      <Formik
        validate={values => {
          const errors = {};

          if (!values.email) {
            errors.email = 'Обязательное поле';
          } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
          ) {
            errors.email = 'Неправильно введен email';
          }

          return errors;
        }}
        initialValues={{ email: '' }}
        onSubmit={({ email }, { setErrors, setSubmitting }) => {
          getPassword(email, setErrors, setSubmitting);
        }}
      >
        {({ errors, isSubmitting }) => (
          <StyledForm>
            <Field
              type="email"
              placeholder="Email"
              name="email"
              component={Input}
            />
            <div>
              <Button
                type="submit"
                disabled={!!Object.keys(errors).length || isSubmitting}
              >
                Отправить
              </Button>
              <CustomLink
                style={{ textAlign: 'right', marginTop: '14px' }}
                onClick={() => openModal({ type: 'signIn' })}
              >
                <MdKeyboardBackspace size={22} />
                Вернуться назад
              </CustomLink>
            </div>
          </StyledForm>
        )}
      </Formik>
    </Content>
  );
}

export default connect(
  state => ({
    socials: selectors.getSocials(state),
  }),
  { ...effects },
)(GetPassword);

const Content = styled.div`
  background: #fff;
  padding: 20px 20px 25px;
  max-width: 420px;
  width: 100%;
  margin: 0 auto;
  h2 {
    text-align: center;
    margin-bottom: 40px;
    font-weight: 500;
  }

  ${media.phone`
    /* width: 300px; */
  `}
`;

const StyledForm = styled(Form)`
  display: grid;
  grid-gap: 32px 0;
`;

const Button = styled.button`
  width: 100%;
  cursor: pointer;
  border: none;
  padding: 0 20px;
  height: 52px;
  color: #fff;
  font-size: 16px;
  background: ${({ theme }) => theme.red};
`;

const CustomLink = styled.div`
  color: #0c729c;
  font-size: 14px;
  margin-top: 12px;
  text-align: left;
  cursor: pointer;
  transition: all 0.2s ease;
  display: flex;
  align-items: center;
  justify-content: start;
  svg {
    margin-right: 10px;
  }
  &:hover {
    opacity: 0.8;
  }
`;
