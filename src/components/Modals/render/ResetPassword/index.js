import React, { useEffect } from 'react';
// import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { effects } from 'redux/ducks/auth.duck';
import { Formik, Field, Form } from 'formik';
import Input from 'components/UI/Input';

function ResetPassword({
  email,
  reset_token,
  closeModal,
  openModal,
  resetPass,
  history,
  ...props
}) {
  useEffect(() => {
    return () => history.replace('', null);
  }, []);

  return (
    <Content>
      <h2>Восстановить пароль</h2>
      <Formik
        validate={values => {
          const errors = {};

          if (values.password.length < 3) {
            errors.password = 'Пароль должен быть больше 3 символов';
          }

          if (
            values.password.length < 3 &&
            values.password_confirmation !== values.password
          ) {
            errors.password = 'Пароли должны совпадать';
          }

          return errors;
        }}
        initialValues={{
          email,
          password_confirmation: '',
          password: '',
        }}
        onSubmit={(values, { setErrors, setSubmitting }) =>
          resetPass({ ...values, reset_token }, setErrors, setSubmitting)
        }
      >
        {({ errors, isSubmitting, ...props }) => (
          <StyledForm>
            <Field
              type="email"
              placeholder="E-mail"
              name="email"
              component={Input}
              disabled
            />
            <Field
              type="password"
              placeholder="Пароль"
              name="password"
              component={Input}
            />
            <Field
              type="password"
              placeholder="Повторите пароль"
              name="password_confirmation"
              component={Input}
            />
            <div>
              <Button
                type="submit"
                disabled={!!Object.keys(errors).length || isSubmitting}
              >
                Отправить
              </Button>
            </div>
            {!!Object.keys(errors).length && <Error>{errors.form}</Error>}
          </StyledForm>
        )}
      </Formik>
    </Content>
  );
}

export default connect(null, { ...effects })(ResetPassword);

const Content = styled.div`
  background: #fff;
  padding: 20px 20px 40px;
  max-width: 420px;
  width: 100%;
  h2 {
    text-align: center;
    margin-bottom: 40px;
  }

  input {
    &:disabled {
      background: rgba(0, 0, 0, 0.043);
    }
  }
`;

const StyledForm = styled(Form)`
  display: grid;
  grid-gap: 32px 0;
`;

const Button = styled.button`
  width: 100%;
  cursor: pointer;
  border: none;
  padding: 0 20px;
  height: 52px;
  color: #fff;
  font-size: 16px;
  background: ${({ theme }) => theme.red};
`;

const Error = styled.div`
  font-size: 14px;
  color: ${({ theme }) => theme.red};
  margin-bottom: 10px;
`;
