import React from 'react';
import styled, { keyframes } from 'styled-components';
import { connect } from 'react-redux';
import { actions, selectors } from 'redux/ducks/user.duck';
import { media } from 'styles';
import { Formik, Field, Form } from 'formik';
import Input from 'components/UI/Input';
import ImageUpload from 'components/UI/ImageUpload';
import { MdAutorenew } from 'react-icons/md';

function EditProfile({ closeModal, profile, loading, updateProfileRequest }) {
  return (
    <Content>
      <h2>Редактировать профиль</h2>
      <Formik
        validate={values => {
          const errors = {};

          if (!values.email) {
            errors.email = 'Обязательное поле';
          } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
          ) {
            errors.email = 'Неправильно введен email';
          }

          if (values.name < 3) {
            errors.name = 'Мало символов';
          }

          if (values.lastName < 3) {
            errors.name = 'Мало символов';
          }

          // if (values.password < 6) {
          //   errors.name = 'Пароль должен быть больше 6 симлово';
          // }

          return errors;
        }}
        initialValues={{
          email: profile.email || '',
          name: profile.name || '',
          last_name: profile.last_name || '',
          image: profile.image || '',
        }}
        enableReinitialize
        onSubmit={async (values, { setSubmitting }) => {
          await updateProfileRequest(values);
          await setSubmitting(false);
        }}
      >
        {({ errors, isSubmitting }) => (
          <StyledForm>
            <Field name="image" component={ImageUpload} />
            <Field
              type="email"
              placeholder="Email"
              name="email"
              component={Input}
            />
            <Field placeholder="Имя" name="name" component={Input} />
            <Field placeholder="Фамилия" name="last_name" component={Input} />

            <Button
              type="submit"
              disabled={!!Object.keys(errors).length || isSubmitting}
            >
              Сохранить
              {loading && <MdAutorenew size={24} color="#fff" />}
            </Button>
          </StyledForm>
        )}
      </Formik>
    </Content>
  );
}

export default connect(
  state => ({
    profile: selectors.getProfile(state),
    loading: selectors.getLoading(state),
  }),
  { ...actions },
)(EditProfile);

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

const Content = styled.div`
  background: #fff;
  padding: 20px 20px 40px;
  max-width: 420px;
  width: 100%;
  text-align: center;
  h2 {
    margin-bottom: 40px;
    font-weight: 500;
  }
`;

const StyledForm = styled(Form)`
  display: grid;
  grid-gap: 24px 0;
`;

const Button = styled.button`
  width: 100%;
  cursor: pointer;
  border: none;
  padding: 0 20px;
  height: 52px;
  color: #fff;
  font-size: 16px;
  background: ${({ theme }) => theme.red};
  display: flex;
  justify-content: center;
  align-items: center;
  & > svg {
    margin-left: 6px;
    animation: ${rotate} 2s linear infinite;
  }
`;
