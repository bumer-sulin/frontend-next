// import { createPortal } from 'react-dom';

// const MainPortal = ({ children, modalRoot }) =>
//   createPortal(children, modalRoot);

// MainPortal.defaultProps = {
//   modalRoot: document.getElementById('modal'),
// };

// export default MainPortal;

import { useRef, useEffect, useState } from 'react';
import { createPortal } from 'react-dom';

const MainPortal = ({ children, selector }) => {
  const ref = useRef();
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    ref.current = document.querySelector(selector);
    setMounted(true);
  }, [selector]);

  return mounted ? createPortal(children, ref.current) : null;
};

export default MainPortal;
