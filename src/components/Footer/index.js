import React from 'react';
import styled from 'styled-components';
import Link from 'next/link';
import { Container, media } from 'styles';
import SocialIcons from 'components/Social';
import { Formik, Field, Form } from 'formik';
import logoFooter from 'assets/images/logo-footer.png';

function validateEmail(value) {
  let error;
  if (!value) {
    error = 'Обязательное поле';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
    error = 'Неправильно введен email';
  }
  return error;
}

const Footer = ({ categories, socials, settings, subscribeRequest }) => (
  <Content>
    <MainContainer>
      <FooterWrap>
        <div>
          <strong>О нас</strong>
          <Link href="/">
            <a>
              <img
                src={logoFooter}
                alt="Главный портал Красного Сулина"
                width="140"
              />
              <h4>
                Главный портал <br /> Красного Сулина
              </h4>
              <p>Официальный сайт медиа-холдинга "Красный Бумер".</p>
            </a>
          </Link>
        </div>
        <div>
          <strong>Рубрики</strong>
          <List>
            {categories.map(({ title, id }) => (
              <Link
                href={{ pathname: '/search', query: { category_id: id } }}
                key={id}
              >
                <a>{title}</a>
              </Link>
            ))}
          </List>
        </div>

        <div>
          <strong>Социальные сети</strong>
          <Social>
            {socials.length > 0 &&
              socials.map(({ id, ...social }) => (
                <SocialIcons key={id} {...social} />
              ))}
          </Social>
          <strong style={{ marginTop: '40px' }}>Подписаться на нас</strong>
          <Subscribe>
            <Formik
              initialValues={{ email: '' }}
              onSubmit={async ({ email }, { resetForm }) => {
                await subscribeRequest(email);
                await resetForm();
              }}
            >
              {({ errors, touched }) => (
                <StyledForm>
                  <Field
                    name="email"
                    placeholder="Введите Email"
                    validate={validateEmail}
                    component="input"
                    type="email"
                  />
                  {errors.email && touched.email && (
                    <Error>{errors.email}</Error>
                  )}
                  <button type="submit">Отправить</button>
                </StyledForm>
              )}
            </Formik>
          </Subscribe>
        </div>
      </FooterWrap>
      {settings.appContacts && (
        <Contacts
          dangerouslySetInnerHTML={{
            __html: settings.appContacts.value,
          }}
        />
      )}
      <BottomContent>
        {settings.appCopyright_footer && settings.appCopyright_footer.value}
      </BottomContent>
    </MainContainer>
  </Content>
);

export default Footer;

const Content = styled.div`
  background: #202124;
  padding: 96px 0 96px;
  ${media.tablet`
    padding: 48px 0 48px;
  `}
`;

const MainContainer = styled.div`
  ${Container}
`;

const FooterWrap = styled.div`
  display: grid;
  grid-template-columns: 0.2fr 0.5fr 0.3fr;
  justify-content: space-between;
  grid-gap: 0 40px;
  & > div {
    &:first-child {
      h4 {
        color: #fff;
        margin-top: 20px;
        line-height: 1.6;
        font-size: 22px;
        margin-bottom: 10px;
        ${media.tablet`
          margin-top: 0;
        `}
      }

      p {
        color: #fff;
        line-height: 1.4;
        font-weight: 400;
        font-size: 12px;
      }

      a {
        ${media.tablet`
          display: block;
          align-items: center;
        `}
      }
    }

    & > strong {
      color: #fff;
      margin-bottom: 30px;
      font-size: 22px;
      display: block;
      position: relative;
      padding-bottom: 14px;
      &:after {
        content: '';
        position: absolute;
        left: 0;
        bottom: 0;
        width: 80px;
        height: 3px;
        background: #fff;
      }
      ${media.tablet`
        font-size: 24px;
      `}
    }
  }

  ${media.tablet`
    grid-template-columns: 1fr;
    grid-gap: 40px 0;
  `}
`;

const List = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 15px 30px;

  a {
    color: #fff;
    font-size: 15px;
    font-weight: 500;
  }

  ${media.phone`
    grid-template-columns: 1fr 1fr;
    grid-gap: 14px 16px;
  `}
`;

const BottomContent = styled.div`
  margin-top: 100px;
  color: #fff;
  font-size: 12px;
  line-height: 22px;
  ${media.tablet`
    margin-top: 60px;
  `}
`;

const Social = styled.div`
  display: grid;
  grid-auto-flow: column;
  justify-content: start;
  grid-gap: 0 16px;
`;

const Subscribe = styled.div`
  position: relative;
  padding-bottom: 10px;
`;

const StyledForm = styled(Form)`
  display: grid;
  grid-template-columns: 1fr auto;
  align-items: center;

  input {
    height: 38px;
    width: 100%;
    font-size: 12px;
    padding-left: 12px;
    color: #ccc;
    border: 1px solid #eee;
    background: #fff;

    &:focus {
      outline: none;
    }
  }

  button {
    font-size: 14px;
    background: ${({ theme }) => theme.red};
    height: 38px;
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 0 10px;
    color: #fff;
    font-weight: 600;
    border: none;
    cursor: pointer;
    transition: all 0.2s ease;
    &:hover {
      opacity: 0.8;
    }
  }
`;

const Error = styled.div`
  padding-left: 10px;
  position: absolute;
  top: 100%;
  left: 0;
  color: ${({ theme }) => theme.red};
  font-size: 12px;
`;

const Contacts = styled.div`
  color: #fff;
  margin-top: 40px;

  & > strong {
    color: #fff;
    margin-bottom: 30px;
    display: block;
    position: relative;
    padding-bottom: 14px;
    font-size: 24px;
    a {
      color: #fff;
    }
    &:after {
      content: '';
      position: absolute;
      left: 0;
      bottom: 0;
      width: 80px;
      height: 3px;
      background: #fff;
    }
  }

  p {
    span {
      font-weight: 600;
    }
    a {
      color: ${({ theme }) => theme.red};
      font-weight: 500;
      text-decoration: underline;
    }
  }
`;
