import React from 'react';
import styled from 'styled-components';
import searchParams from 'lib/helpers/searchCategory';

const TagsList = ({ tags }) => (
  <Content>
    {tags.map(({ title, id, color }) => (
      <Item
        color={color}
        to={{
          pathname: '/search',
          search: searchParams(null, null, [id]),
        }}
        key={id}
      >
        {title}
      </Item>
    ))}
  </Content>
);

export default TagsList;

const Content = styled.div`
  display: flex;
  flex-wrap: wrap;
  a {
    margin-right: 10px;
  }
`;

const Item = styled.a`
  background: ${({ color, theme }) => color || theme.red};
  font-size: 13px;
  padding: 3px 10px;
  font-weight: 600;
  color: #fff;
  border-radius: 2px;
  height: 26px;
  display: inline-flex;
  align-items: center;
  margin-bottom: 10px;
`;
