import React from 'react';
import styled from 'styled-components';

const Input = ({ field, form, title, type, ...props }) => (
  <FormGroup className={form.errors[field.name] ? 'error' : ''}>
    {title && <Label>{title}</Label>}
    <FormControl>
      <InputContent type={type} {...field} {...props} />
      {form.touched[field.name] && form.errors[field.name] && (
        <Error>{form.errors[field.name]}</Error>
      )}
    </FormControl>
  </FormGroup>
);

Input.defaultProps = {
  type: 'text',
  placeholder: '',
};

export default Input;

const FormGroup = styled.div`
  position: relative;

  &.error {
    input {
      border-color: rgb(250, 134, 134);
      &:focus {
        box-shadow: inset 0 2px 2px rgba(250, 134, 134, 0.075),
          0 0 0 3px rgba(250, 134, 134, 0.1);
      }
    }
    label {
      color: rgb(250, 134, 134);
    }
  }
`;

const Label = styled.label``;

const FormControl = styled.div`
  position: relative;
`;

const Error = styled.span`
  color: rgba(250, 134, 134);
  /* position: absolute; */
  /* left: 0; */
  /* top: 100%; */
  display: block;
  margin-top: 5px;
  font-size: 14px;
`;

const InputContent = styled.input`
  height: 52px;
  background: transparent;
  border: 1px solid rgba(0, 0, 0, 0.15);
  padding-left: 20px;
  width: 100%;
  font-size: 16px;
  border-color: ${({ error }) => (error ? '#007eff' : '#ccc')};
  &:focus {
    border-color: #007eff;
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075),
      0 0 0 3px rgba(0, 126, 255, 0.1);
    outline: none;
  }
`;

const TextareaContent = styled.textarea`
  padding: 0.5rem;
  font-size: 16px;
  width: 100%;
  display: block;
  border-radius: 4px;
  border: 1px solid #ccc;
  border-color: ${({ error }) => (error ? '#007eff' : '#ccc')};
  &:focus {
    border-color: #007eff;
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075),
      0 0 0 3px rgba(0, 126, 255, 0.1);
    outline: none;
  }
`;
