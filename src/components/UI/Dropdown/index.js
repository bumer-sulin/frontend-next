import React from "react";
import styled from "styled-components";

const Dropdown = ({ isOpen, onClick, id, children }) => (
  <Content>
    <ButtonDropdown onClick={onClick} className="dropdownBtn">
      <span />
      <span />
      <span />
    </ButtonDropdown>
    <DropdownList className={isOpen ? "open" : ""}>{children}</DropdownList>
  </Content>
);

export default Dropdown;

const Content = styled.div`
  position: relative;
`;

const ButtonDropdown = styled.div`
  width: 40px;
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  border: 2px #6c757d solid;
  border-radius: 4px;
  padding: 4px;
  cursor: pointer;
  & > span {
    width: 6px;
    height: 6px;
    display: block;
    background: #6c757d;
    border-radius: 50%;
    margin-right: 2px;
    pointer-events: none;
    &:last-child {
      margin-right: 0;
    }
  }
`;

const DropdownList = styled.div`
  position: absolute;
  will-change: transform;
  top: 0px;
  right: 0px;
  transform: translate3d(0px, 40px, 0px);
  min-width: 160px;
  padding: 8px 0;
  margin: 5px 0 0;
  font-size: 14px;
  background: #fff;
  color: #212529;
  text-align: left;
  list-style: none;
  background-clip: padding-box;
  border: 1px solid rgba(0, 0, 0, 0.15);
  border-radius: 0.25rem;
  z-index: 10;
  opacity: 0;
  visibility: hidden;
  overflow: hidden;
  transition: all 0.1s ease;

  &.open {
    opacity: 1;
    visibility: visible;
  }
`;
