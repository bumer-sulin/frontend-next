import React, { useState } from 'react';
import styled from 'styled-components';
import { IoIosPerson } from 'react-icons/io';

function ImageUpload({
  setFieldValue,
  field,
  form,
  form: { touched, errors },
  ...props
}) {
  const [image, setFile] = useState({
    file: null,
    imagePreviewUrl: field.value || null,
  });

  function handleImageChange(e) {
    e.preventDefault();

    const reader = new FileReader();
    const file = e.target.files[0];

    if (file) {
      reader.onloadend = async () => {
        await setFile({
          file,
          imagePreviewUrl: reader.result,
        });
        await form.setFieldValue(field.name, reader.result);
      };

      reader.readAsDataURL(file);
    }
  }

  const { imagePreviewUrl } = image;

  return (
    <StyledEditAvatar>
      <ImageContainer>
        {imagePreviewUrl ? (
          <img src={imagePreviewUrl} />
        ) : (
          <IoIosPerson size="44" />
        )}
      </ImageContainer>
      <StyledUploadButton htmlFor="file">
        <input
          type="file"
          accept="image/*"
          id="file"
          onChange={handleImageChange}
        />
        <Button className="ac-btn ac-btn_primary ac-btn_small" type="button">
          Изменить
        </Button>
      </StyledUploadButton>
    </StyledEditAvatar>
  );
}

export default ImageUpload;

const ImageContainer = styled.div`
  width: 100px;
  height: 100px;
  font-size: 18px;
  border-radius: 50%;
  overflow: hidden;
  margin-right: 15px;
  display: flex;
  justify-content: center;
  align-items: center;
  & > img {
    object-fit: cover;
    width: 100%;
    height: 100%;
    display: block;
  }
`;

const StyledEditAvatar = styled.div`
  display: flex;
  align-items: center;
`;

const StyledUploadButton = styled.label`
  cursor: pointer;
  & > input {
    position: absolute;
    opacity: 0;
    overflow: hidden;
    visibility: hidden;
    width: 0;
    height: 0;
  }
`;

const Button = styled.button`
  width: 100%;
  cursor: pointer;
  border: none;
  padding: 0 20px;
  height: 34px;
  color: #fff;
  font-size: 14px;
  background: ${({ theme }) => theme.red};
  pointer-events: none;
`;
