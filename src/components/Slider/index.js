import React, { useRef } from 'react';
import { connect } from 'react-redux';
import { actions } from 'redux/ducks/modals.duck';
import styled from 'styled-components';
import SwiperCore, {
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Virtual,
  Autoplay,
} from 'swiper';

import { Swiper, SwiperSlide } from 'swiper/react';

import AlternativeArticle from 'components/AlternativeArticle';
import { MdArrowBack, MdArrowForward } from 'react-icons/md';
import { ResetDefaultButton, media } from 'styles';

SwiperCore.use([Navigation, Pagination, Scrollbar, A11y, Virtual, Autoplay]);

function Slider({ items, gallery, openModal }) {
  const ref = useRef(null);

  function onMouseEnter() {
    // swiper.current.swiper.autoplay.pause();
  }

  function onMouseLeave() {
    // swiper.current.swiper.autoplay.run();
  }

  function handleClick(index, galleryImages) {
    openModal({
      type: 'gallery',
      args: {
        galleryImages,
        index,
      },
    });
  }

  return (
    items.length > 0 && (
      <ContainerCarousel
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
      >
        <Swiper
          ref={ref}
          spaceBetween={50}
          autoplay={{
            delay: 5000,
            disableOnInteraction: true,
          }}
          // virtual
          slidesPerView={1}
          navigation={{
            nextEl: '.button-next',
            prevEl: '.button-prev',
          }}
          scrollbar={{ draggable: true }}
        >
          <div>
            <Button className="button-prev">
              <MdArrowBack size={25} />
            </Button>
            <Button className="button-next">
              <MdArrowForward size={25} />
            </Button>
          </div>

          {items.map((slide, index) => (
            <SwiperSlide key={slide.id}>
              {gallery && (
                <OpenGallery onClick={() => handleClick(index, items)} />
              )}
              <AlternativeArticle {...slide} index={index} />
            </SwiperSlide>
          ))}
        </Swiper>
      </ContainerCarousel>
    )
  );
}

export default connect(null, { ...actions })(Slider);

const ContainerCarousel = styled.div`
  max-width: 100%;
  width: 100%;
  position: relative;
  margin-bottom: 40px;
  overflow: hidden;
  .button-prev,
  .button-next {
    position: absolute;
    bottom: 0;
    right: 0;
    z-index: 100;
    padding: 20px;
    color: #fff;
    transition: all 0.2s ease;
    &:hover {
      background: rgba(255, 255, 255, 0.1);
    }

    ${media.phone`
      top: 20px;
      bottom: inherit;

      &:first-child {
        padding: 20px 40px 20px 5px;
      }
      &:last-child {
        padding: 20px 5px 20px 40px;
      }
      &:hover {
        background: none;
      }
  `}
  }

  .button-prev {
    right: 65px;
    ${media.phone`
      right: inherit;
      left: 0;
    `}
  }
  .button-next {
    ${media.phone`
      right: 0;
    `}
  }
`;

const Button = styled.button`
  ${ResetDefaultButton}
  padding: 20px;
  color: #fff;
  transition: all 0.2s ease;
  &:hover {
    background: rgba(255, 255, 255, 0.1);
  }

  ${media.phone`
    &:first-child {
      padding: 20px 40px 20px 5px;
    }
    &:last-child {
      padding: 20px 5px 20px 40px;
    }
    &:hover {
      background: none;
    }
  `}
`;

const OpenGallery = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  z-index: 999;
  cursor: pointer;
  transition: all 0.3s ease;
  &:hover {
    opacity: 0.8;
  }
`;
