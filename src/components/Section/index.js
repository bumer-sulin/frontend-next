import React from 'react';
import styled from 'styled-components';
import Link from 'next/link';
import { MdKeyboardArrowRight } from 'react-icons/md';

const Section = ({ title, noMore, children, categoryId }) => (
  <Content>
    <Title>
      <h3>{title}</h3>
      {!noMore && (
        <Link
          href={{
            pathname: '/search',
            query: {
              categoryId,
            },
          }}
        >
          <StyledLink>
            Далее
            <MdKeyboardArrowRight size={24} />
          </StyledLink>
        </Link>
      )}
    </Title>
    <div>{children}</div>
  </Content>
);

export default Section;

Section.defaultProps = {
  noMore: false,
};

const Content = styled.div`
  margin-bottom: 60px;
`;

const Title = styled.div`
  border-bottom: 1px solid rgba(0, 0, 0, 0.15);
  margin-bottom: 40px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  h3 {
    font-size: 20px;
    margin-bottom: 0;
    font-weight: 400;
    padding-bottom: 20px;
    border-bottom: 3px solid #f13d37;
    display: inline-block;
    margin-bottom: -2px;
  }
`;

const StyledLink = styled.a`
  display: flex;
  align-items: center;
  /* text-transform: uppercase; */
  font-size: 15px;
  color: rgba(0, 0, 0, 0.54);
`;
