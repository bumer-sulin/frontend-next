import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { selectors, actions } from 'redux/ducks/main.duck';
import { LoadingDefault } from 'components/UI/Loading';
import { Formik, Field, Form } from 'formik';
import { MdEmail } from 'react-icons/md';
import Section from '../Section';

function validateEmail(value) {
  let error;
  if (!value) {
    error = 'Обязательное поле';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
    error = 'Неправильно введен email';
  }
  return error;
}

const Subscribe = ({ subscribe, settings, loading, subscribeRequest }) => (
  <Section title="Подписаться рассылку" noMore>
    {loading ? (
      <LoadingDefault />
    ) : (
      <Content>
        <MailIcon>
          <MdEmail size={30} />
        </MailIcon>
        <h4>{settings.appSubscription && settings.appSubscription.title}</h4>
        <p>{settings.appSubscription && settings.appSubscription.value}</p>
        <Formik
          initialValues={{ email: '' }}
          onSubmit={async ({ email }, { resetForm }) => {
            await subscribeRequest(email);
            await resetForm();
          }}
        >
          {({ errors, touched }) => (
            <StyledForm>
              <Field name="email" validate={validateEmail} />
              {errors.email && touched.email && <Error>{errors.email}</Error>}
              <button type="submit">Отправить</button>
            </StyledForm>
          )}
        </Formik>
      </Content>
    )}
  </Section>
);

export default connect(
  state => ({
    subscribe: selectors.getSubscribe(state),
    settings: selectors.getSettings(state),
    loading: selectors.getLoading(state),
  }),
  { ...actions },
)(Subscribe);

const Content = styled.div`
  padding: 15px 15px 24px;
  border: 1px solid #eee;
  text-align: center;
  position: relative;

  h4 {
    font-size: 20px;
    font-weight: normal;
  }
  p {
    font-weight: normal;
    color: rgba(0, 0, 0, 0.5);
  }
`;

const StyledForm = styled(Form)`
  display: grid;
  grid-template-columns: 1fr auto;
  align-items: center;
  margin-top: 20px;
  position: relative;
  padding-bottom: 10px;

  input {
    height: 38px;
    width: 100%;
    font-size: 14px;
    padding-left: 12px;
    color: #000;
    border: 1px solid #eee;
    background: #fff;
    transition: all 0.1s ease;

    &:focus {
      outline: none;
      border: 1px solid #e0dada;
      background: #f5f5f5;
    }
    &:hover {
      border: 1px solid #e0dada;
      background: #f5f5f5;
    }
  }

  button {
    font-size: 14px;
    background: ${({ theme }) => theme.red};
    height: 38px;
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 0 10px;
    color: #fff;
    font-weight: 600;
    border: none;
    cursor: pointer;
    transition: all 0.2s ease;
    &:hover {
      opacity: 0.8;
    }
  }
`;

const MailIcon = styled.div`
  width: 60px;
  height: 60px;
  background: ${({ theme }) => theme.red};
  display: inline-flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 30px;
  color: #fff;
`;

const Error = styled.div`
  padding-left: 10px;
  position: absolute;
  top: 100%;
  left: 0;
  color: ${({ theme }) => theme.red};
  font-size: 12px;
`;
