import React, { useLayoutEffect } from 'react';
import { useRouter } from 'next/router';
import styled from 'styled-components';
import Section from 'components/Section';
import SmallArticle from 'components/SmallArticle';
import TagsList from 'components/TagsList';
import CategoriesList from 'components/CategoriesList';
import Subscribe from 'components/Subscribe';
import { media } from 'styles';

function Sidebar({ populars, categories, tags, promotions }) {
  const router = useRouter();

  useLayoutEffect(() => {
    setTimeout(() => {
      const s = document.createElement('script');
      s.type = 'text/javascript';
      s.async = true;
      s.src =
        'https://www.gismeteo.ru/api/informer/getinformer/?hash=Y1vFggNjaC020p';
      document.body.append(s);
    }, 6000);
  }, []);

  return (
    <Content>
      <Section title="Популярные новости" noMore>
        <TopCountList>
          {populars &&
            populars.map((popular, index) => {
              const { id } = popular;

              return (
                <TopCountItem key={id}>
                  <TopCount>{`0${index + 1}`}</TopCount>
                  <SmallArticle {...popular} />
                </TopCountItem>
              );
            })}
        </TopCountList>
      </Section>
      <Pogoda>
        <div
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{
            __html: `
            <div id="gsInformerID-Y1vFggNjaC020p" class="gsInformer" style="width:330px;height:235px">
    <div class="gsIContent">
        <div id="cityLink">
            <a href="https://www.gismeteo.ru/weather-krasny-sulin-12723/" target="_blank" title="Погода в Красном Сулине"><img src="https://ost1.gismeteo.ru/assets/flat-ui/img/gisloader.svg" width="24" height="24" alt="Погода в Красном Сулине"></a>
        </div>
        <div class="gsLinks">
            <table>
                <tr>
                    <td>
                        <div class="leftCol">
                            <a href="https://www.gismeteo.ru/" target="_blank" title="Погода в Красном Сулине">
                                <img alt="Погода в Красном Сулине" src="https://ost1.gismeteo.ru/assets/flat-ui/img/logo-mini2.png" align="middle" border="0" width="11" height="16" />
                                <img src="https://ost1.gismeteo.ru/assets/flat-ui/img/informer/gismeteo.svg" border="0" align="middle" style="left: 5px; top:1px">
                            </a>
                            </div>
                            <div class="rightCol">
                                <a href="https://www.gismeteo.ru/weather-krasny-sulin-12723/2-weeks/" target="_blank" title="Погода в Красном Сулине на 2 недели">
                                    <img src="https://ost1.gismeteo.ru/assets/flat-ui/img/informer/forecast-2weeks.ru.svg" border="0" align="middle" style="top:auto" alt="Погода в Красном Сулине на 2 недели">
                                </a>
                            </div>
                                            </td>
                </tr>
            </table>
        </div>
    </div>
</div>
`,
          }}
        />
      </Pogoda>
      {!router.pathname.includes('/search') && (
        <Section title="Категория" noMore>
          <CategoriesList categories={categories} />
          {tags.length > 0 && (
            <TagsList tags={tags.map(e => ({ ...e, to: '/search' }))} />
          )}
        </Section>
      )}
      {promotions && (
        <Advertising
          style={{
            backgroundImage: promotions.sidebar
              ? `url(${promotions.sidebar.image})`
              : '',
          }}
        >
          {promotions.sidebar ? (
            <a href={promotions.sidebar.link} />
          ) : (
            <h4>Реклама</h4>
          )}
        </Advertising>
      )}
      <Subscribe />
    </Content>
  );
}

export default Sidebar;

const Content = styled.div`
  ${media.desktop`
    display: none;
  `}
`;

const TopCountList = styled.div`
  display: grid;
  grid-gap: 22px 0;
  padding-left: 20px;
  padding-right: 20px;
`;

const TopCountItem = styled.div`
  display: grid;
  grid-template-columns: auto 1fr;
  grid-gap: 0 16px;
`;

const TopCount = styled.div`
  font-size: 34px;
  color: rgba(0, 0, 0, 0.15);
`;

const Advertising = styled.div`
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  background-color: ${({ theme }) => theme.red};
  min-height: 400px;
  margin-bottom: 40px;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  h4 {
    color: #fff;
    font-size: 28px;
  }
  a {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 1;
  }
`;

const Pogoda = styled.div`
  margin-bottom: 40px;
`;
