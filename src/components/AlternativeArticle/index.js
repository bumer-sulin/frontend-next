import React from 'react';
import Link from 'next/link';
import styled from 'styled-components';
import { media } from 'styles';

function AlternativeArticle({
  title,
  image,
  category,
  published_at,
  id,
  slug,
  galleryImages,
  openModal,
  index,
}) {
  return (
    <Content style={{ backgroundImage: `url(${image})` }}>
      <MainContent>
        <Header>
          <Label color={category.color}>{category.title}</Label>
          <span>{published_at}</span>
        </Header>
        <h3>
          {galleryImages ? (
            title
          ) : (
            <Link href="/article/[...slug]" as={`/article/${id}/${slug}`}>
              <a>{title}</a>
            </Link>
          )}
        </h3>
      </MainContent>
    </Content>
  );
}

export default AlternativeArticle;

const Content = styled.div`
  height: 450px;
  position: relative;
  width: 100%;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  display: grid;
  align-items: end;
  padding: 20px;
  &:after {
    content: '';
    display: block;
    height: 60%;
    background: linear-gradient(
      to top,
      rgba(0, 0, 0, 0.8) 0%,
      rgba(0, 0, 0, 0.6) 50%,
      rgba(0, 0, 0, 0.4) 75%,
      rgba(0, 0, 0, 0) 100%
    );
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    z-index: 10;
  }

  ${media.phone`
    height: 420px;
    padding: 15px;
    &:after {
      height: 100%;
      background: linear-gradient(
        to top,
        rgba(0, 0, 0, 0.8) 0%,
        rgba(0, 0, 0, 0.6) 25%,
        rgba(0, 0, 0, 0.4) 50%,
        rgba(0, 0, 0, 0.2) 75%,
        rgba(0, 0, 0, 0) 100%
      );
    }
  `}
`;

const Label = styled.div`
  background: ${({ theme, color }) => color || theme.red};
  font-size: 13px;
  padding: 3px 10px;
  font-weight: 600;
  color: #fff;
  border-radius: 2px;
  height: 26px;
  display: flex;
  align-items: center;
`;

const Header = styled.div`
  display: grid;
  align-items: center;
  justify-content: start;
  grid-auto-flow: column;
  grid-gap: 20px;
  margin-bottom: 15px;
  color: #fff;
  & > span {
    font-size: 14px;
    font-weight: 400;
  }
  ${media.phone`
    grid-gap: 14px 0;
    justify-items: start;
    grid-auto-flow: row;
    margin-bottom: 20px;
  `}
`;

const MainContent = styled.div`
  position: relative;
  z-index: 11;
  padding-right: 15%;
  h3 {
    font-size: 22px;
    line-height: 31px;
    margin-bottom: 0;
    font-weight: 500;
    color: #fff;
    a {
      color: #fff;
    }
  }

  ${media.phone`
    padding-right: 0;
    h3 {
      font-size: 16px;
      line-height: 24px;
    }
  `}
`;
