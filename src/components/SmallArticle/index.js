import React from 'react';
import Link from 'next/link';
import styled from 'styled-components';
import { MdRemoveRedEye, MdComment } from 'react-icons/md';

const SmallArticle = ({ title, count, id, slug }) => (
  <Content>
    <MainContent>
      <h3>
        <Link href={`/article/${id}/${slug}`}>
          <a>{title}</a>
        </Link>
      </h3>
      <Stats>
        <StatItem>
          <MdRemoveRedEye />
          <span>{count.views}</span>
        </StatItem>
        <StatItem>
          <MdComment />
          <span>{count.comments}</span>
        </StatItem>
      </Stats>
    </MainContent>
  </Content>
);

export default SmallArticle;

const Content = styled.div``;

const MainContent = styled.div`
  h3 {
    font-size: 16px;
    line-height: 22px;
    margin-bottom: 10px;
    font-weight: 500;
  }
`;

const Stats = styled.div`
  display: inline-grid;
  grid-auto-flow: column;
  grid-gap: 0 11px;
`;

const StatItem = styled.div`
  display: flex;
  align-items: center;
  color: #777777;
  span {
    font-size: 12px;
    margin-left: 5px;
  }
`;
