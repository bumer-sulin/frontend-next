import React from 'react';
import Link from 'next/link';
import styled from 'styled-components';

const CategorisList = ({ categories }) => (
  <Content>
    {categories.slice(0, 5).map(({ title, id, color, articles }) => (
      <Item key={id}>
        <Link
          href={{
            pathname: '/search',
            query: {
              category_id: id,
            },
          }}
        >
          <Name>{title}</Name>
        </Link>
        <Count color={color}>{articles}</Count>
      </Item>
    ))}
  </Content>
);

export default CategorisList;

const Content = styled.div`
  margin-bottom: 40px;
`;

const Item = styled.div`
  border-bottom: 1px #e1e1e1 solid;
  padding-bottom: 10px;
  margin-bottom: 10px;
  display: grid;
  grid-auto-flow: column;
  grid-template-columns: 0.8fr 0.2fr;
  grid-gap: 0 16px;
`;

const Name = styled.a`
  font-weight: 500;
`;

const Count = styled.div`
  justify-self: end;
  background: ${({ color, theme }) => color || theme.red};
  font-size: 13px;
  padding: 3px 5px;
  font-weight: 600;
  color: #fff;
  border-radius: 2px;
  /* height: 26px; */
  display: inline-flex;
  align-items: center;
`;
