import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import { Container, media } from 'styles';
import { connect } from 'react-redux';
import { selectors, actions } from 'redux/ducks/main.duck';
import {
  selectors as selectorsUser,
  actions as actionsUser,
} from 'redux/ducks/user.duck';
import {
  selectors as selectoresPromotions,
  actions as actionsPromotions,
} from 'redux/ducks/promotions.duck';
import { actions as actionsModals } from 'redux/ducks/modals.duck';
import styled from 'styled-components';
import { LoadingFixed } from 'components/UI/Loading';
import Header from 'components/Header';
import Footer from 'components/Footer';
import Sidebar from 'components/Sidebar';
import { getHash, setHash } from 'lib/helpers/token';

const Index = props => {
  const { query, replace, pathname } = useRouter();
  const { loading, settings, children } = props;

  useEffect(() => {
    const {
      fetchCategoriesRequest,
      fetchSettingsRequest,
      fetchPopularsRequest,
      fetchTagsRequest,
      fetchSocialsRequest,
      fetchPromotionsRequest,
      fetchProfileRequest,
      openModal,
    } = props;

    const gettingHash = () => {
      if (query.hash) {
        setHash(query.hash);
        replace.replace('/');
      }

      return getHash();
    };

    // if (query.reset_token && query.email) {
    //   openModal({
    //     type: 'resetPassword',
    //     args: [...searchParams.entries()].reduce((acc, current) => {
    //       return { ...acc, [current[0]]: current[1] };
    //     }, {}),
    //   });
    // }

    if (gettingHash()) {
      fetchProfileRequest(gettingHash());
    }

    fetchPromotionsRequest();
    fetchCategoriesRequest();
    fetchSettingsRequest();
    fetchPopularsRequest();
    fetchTagsRequest();
    fetchSocialsRequest();

    return () => {};
  }, []);

  return (
    <>
      {loading && <LoadingFixed />}

      <Content id="wrapper">
        <Header {...props} />
        <MainContent>
          <MainContainer>
            {children}
            {!pathname.includes('/article') && <Sidebar {...props} />}
          </MainContainer>
        </MainContent>
        <Footer {...props} />
        {/* <Modal /> */}
      </Content>
    </>
  );
};

export default connect(
  state => ({
    categories: selectors.getCategories(state),
    populars: selectors.getPopulars(state),
    loading: selectors.getLoading(state),
    tags: selectors.getTags(state),
    socials: selectors.getSocials(state),
    settings: selectors.getSettings(state),
    promotions: selectoresPromotions.getPromotions(state),
    loadingPromotions: selectoresPromotions.getLoading(state),
    profile: selectorsUser.getProfile(state),
  }),
  { ...actions, ...actionsPromotions, ...actionsUser, ...actionsModals },
)(Index);

const Content = styled.div``;

const MainContent = styled.div`
  ${Container}
  padding-top: 40px;
  margin-bottom: 60px;
  width: 100%;
`;

const MainContainer = styled.div`
  display: grid;
  width: 100%;
  margin: 0 auto;
  grid-template-columns: 780px auto;
  grid-gap: 0 30px;
  ${media.desktop`
    grid-template-columns: 100%;
  `}
`;
