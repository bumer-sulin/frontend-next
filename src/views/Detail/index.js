import React, { useLayoutEffect, useEffect, useRef, useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { connect } from 'react-redux';
import { selectors as selectorsMain } from 'redux/ducks/main.duck';
import { selectors, actions } from 'redux/ducks/article.duck';
import {
  selectors as selectoresPromotions,
  actions as actionsPromotions,
} from 'redux/ducks/promotions.duck';
import { selectors as selectoresAuth } from 'redux/ducks/auth.duck';
import { LoadingDefault } from 'components/UI/Loading';
import styled from 'styled-components';
import { MdRemoveRedEye, MdComment, MdArrowUpward } from 'react-icons/md';
import { media } from 'styles';
// import { Helmet } from 'react-helmet';
import SocialIcons from 'components/Social';
import TagList from 'components/TagsList';
import Testimonials from './Testimonials';
import useScrollTop from './hooks/useScrollTop';

function Detail(props) {
  const {
    fetchDetailArticleRequest,
    fetchDetailCommentRequest,
    loading,
    article,
    socials,
    promotions,
    color,
  } = props;

  const scrollRef = useRef(null);
  const detailElement = useRef(null);

  const router = useRouter();
  const { scrollTop } = useScrollTop({ scrollRef });

  function handleScrollTop() {
    scrollRef.current.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
      inline: 'nearest',
    });
  }

  useEffect(() => {
    if (detailElement.current) {
      const images = detailElement.current.querySelectorAll('img');
      images.forEach(e => {
        const attr = e.getAttribute('alt');

        if (attr) {
          const span = document.createElement('span');
          span.innerHTML = attr;
          span.className = 'inner-title-img';
          e.parentNode.appendChild(span);
        }
      });
    }
  }, [loading]);

  useEffect(() => {
    if (!loading && article) {
      scrollRef.current.scrollIntoView({
        // behavior: 'smooth',
        block: 'start',
        inline: 'nearest',
      });
    }
  }, [loading]);

  useEffect(() => {
    if (article && article.count.comments) {
      fetchDetailCommentRequest(router.query.slug[0]);
    }
  }, [article]);

  useEffect(() => {
    fetchDetailArticleRequest(router.query.slug[0]);
  }, []);

  return loading ? (
    <LoadingDefault />
  ) : (
    (article && (
      <Content>
        <Anchor ref={scrollRef} />
        <ButtonScrollTop isActive={scrollTop} onClick={handleScrollTop}>
          <MdArrowUpward size={24} />
        </ButtonScrollTop>
        <Head>
          <title>{article.title}</title>
          <meta name="description" content={article.description} />
          {article.keywords && (
            <meta name="keywords" content={article.keywords} />
          )}
        </Head>
        <Title>
          <MainImage style={{ backgroundImage: `url(${article.image})` }}>
            {article.image_author && <span>Фото: {article.image_author}</span>}
          </MainImage>
          <SubTitle>
            <div>
              <Social>
                {socials.map(({ id, ...social }) => (
                  <SocialIcons key={id} {...social} />
                ))}
              </Social>

              <Stats>
                <StatItem>
                  <MdRemoveRedEye />
                  <span>{article.count.views}</span>
                </StatItem>
                <StatItem>
                  <MdComment />
                  <span>{article.count.comments}</span>
                </StatItem>
              </Stats>
            </div>

            <Info>
              <strong>Автор: {article.author || 'Неизвестно'}</strong>
              <span>{article.published_at}</span>
            </Info>
          </SubTitle>
          <ArticleTitle>
            <CategoryItem color={color}>{article.category.title}</CategoryItem>
            <h1>{article.title}</h1>
          </ArticleTitle>
        </Title>

        <InnerContent
          ref={detailElement}
          dangerouslySetInnerHTML={{ __html: article.text }}
        />

        <Bottom>
          <div>
            <div>
              <h4>Автор Статьи</h4>
              <span>{article.author || 'Неизвестно'}</span>
            </div>
            <Tags>
              <h4>Теги</h4>
              {article.tags.length > 0 ? (
                <TagList tags={article.tags} />
              ) : (
                <p>Нет тегов</p>
              )}
            </Tags>
          </div>
          <Social>
            {socials.map(({ id, ...social }) => (
              <SocialIcons key={id} {...social} />
            ))}
          </Social>
        </Bottom>
        <Testimonials {...props} />
        {promotions && promotions.article ? (
          <a href={promotions.article.link}>
            <img src={promotions.article.image} />
          </a>
        ) : (
          <Banner>
            <h4>Здесь может быть ваша реклама</h4>
          </Banner>
        )}
      </Content>
    )) ||
      'Нет такой записи'
  );
}

export default connect(
  state => ({
    loading: selectors.getLoading(state),
    article: selectors.getArticle(state),
    comments: selectors.getComments(state),
    socials: selectorsMain.getSocials(state),
    promotions: selectoresPromotions.getPromotions(state),
    loadingPromotions: selectoresPromotions.getLoading(state),
    auth: selectoresAuth.getHash(state),
  }),
  { ...actions, ...actionsPromotions },
)(Detail);

const Content = styled.div`
  max-width: 860px;
  margin: 0 auto;
  position: relative;

  p {
    line-height: 32px;
    font-weight: 300;
    margin: 0px 0px 20px;
    font-size: 18px;
  }
`;

const ArticleTitle = styled.div`
  position: relative;
  margin-bottom: 30px;
  &:after {
    content: '';
    margin-top: 20px;
    display: block;
    width: 120px;
    height: 3px;
    background: ${({ theme }) => theme.red};
  }
  h1 {
    font-size: 36px;
    font-weight: 500;
    margin-bottom: 15px;
    line-height: 43px;
    ${media.phone`
      font-size: 32px;
    `}
  }
`;

const MainImage = styled.div`
  background: #e1e1e1;
  min-height: 500px;
  width: 100%;
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  margin-bottom: 85px;
  position: relative;

  span {
    position: absolute;
    bottom: -45px;
    left: 0;
    display: block;
    font-size: 16px;
    font-weight: 500;
    color: #a1a7af;
  }

  ${media.tablet`
    min-height: 440px;
  `}
  ${media.phone`
    min-height: 260px;
  `}
`;

const InnerContent = styled.div`
  line-height: 32px;
  font-weight: 300;
  margin: 0px 0px 20px;
  font-size: 18px;

  a {
    color: ${({ theme }) => theme.red};
    text-decoration: underline;
    font-weight: 400;
  }

  strong,
  b {
    font-weight: 500;
  }

  i {
    font-style: italic;
  }

  .detail--iframe {
    position: relative;
    padding-bottom: 56.25%;
    padding-top: 30px;
    height: 0;
    overflow: hidden;

    iframe,
    object,
    embed {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
    }
  }

  blockquote {
    position: relative;
    padding: 20px 25px 20px;
    background: rgba(0, 0, 0, 0.04);
    font-size: 19px;
    font-weight: 300;
    line-height: 1.6;
    font-style: italic;
    margin: 25px 0;
    ${media.phone`
      margin: 25px -20px;
    `}
  }

  img {
    display: block;
  }

  .inner-title-img {
    /* display: block; */
    font-weight: 500;
    /* background: rgba(0, 0, 0, 0.043); */
    /* padding: 2px 8px; */
    font-size: 14px;
    border-radius: 4px;
    display: inline-block;
    margin-top: 15px;
    color: rgba(0, 0, 0, 0.7);
  }

  h3,
  h2 {
    margin-top: 40px;
  }

  ${media.phone`
    font-size: 16px;
    line-height: 28px;

    p {
      font-size: 16px;
      line-height: 28px;
    }
  `}
`;

const Title = styled.div`
  margin-bottom: 60px;
`;

const Info = styled.div`
  text-align: right;
  strong,
  span {
    display: block;
  }
  strong {
    font-size: 16px;
    color: #222;
    margin-bottom: 8px;
    font-weight: 500;
  }

  span {
    font-size: 14px;
    font-weight: 400;
    color: #777;
  }
`;

const SubTitle = styled.div`
  display: grid;
  justify-content: space-between;
  grid-auto-flow: column;
  width: 100%;
  position: relative;
  padding: 30px 0;
  margin-bottom: 60px;
  &:after,
  &:before {
    content: '';
    display: block;
    position: absolute;
    height: 3px;
    background: ${({ theme }) => theme.red};
    width: 15%;
    ${media.phone`
      width: 85%;
    `}
  }
  &:after {
    top: 0;
    left: 0;
  }
  &:before {
    bottom: 0;
    right: 0;
  }
  & > div {
    &:first-child {
      display: grid;
      align-items: center;
      justify-content: start;
      grid-gap: 0 25px;
      grid-auto-flow: column;
    }
  }

  ${media.phone`
    justify-content: start;
    grid-template-columns: 1fr;
    grid-auto-flow: row;
    grid-gap: 16px 0;
    & > div {
      &:first-child {
        grid-auto-flow: row;
        grid-gap: 12px 0;
      }
    }
  `}
`;

const Social = styled.div`
  display: grid;
  grid-auto-flow: column;
  justify-content: start;
  grid-gap: 0 10px;
`;

const Stats = styled.div`
  display: grid;
  justify-content: start;
  grid-auto-flow: column;
  grid-gap: 0 11px;
  width: 100%;
`;

const StatItem = styled.div`
  display: flex;
  align-items: center;
  color: #777777;
  span {
    font-size: 15px;
    margin-left: 5px;
  }
`;

const Bottom = styled.div`
  margin-top: 40px;
  padding: 40px 0;
  border-top: 1px #eee solid;
  border-bottom: 1px #eee solid;
  display: grid;
  grid-auto-flow: column;
  justify-content: space-between;
  align-items: end;
  h4 {
    margin-bottom: 10px;
    font-size: 16px;
    color: #222;
  }

  & > div {
    display: grid;
    & > div {
      &:first-child {
        margin-bottom: 20px;
        span {
          font-size: 14px;
          font-weight: 400;
        }
      }
    }
  }

  ${media.phone`
    padding: 20px 0;
    grid-auto-flow: row;
    grid-gap: 36px 0;
  `}
`;

const Tags = styled.div`
  max-width: 400px;
`;

const Banner = styled.div`
  margin-top: 25px;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  height: 140px;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  h4 {
    color: #fff;
    font-size: 28px;
  }
  a {
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    z-index: 1;
  }
`;

const CategoryItem = styled.div`
  background: ${({ color }) => color || '#f13d37'};
  font-size: 15px;
  padding: 3px 10px;
  font-weight: 600;
  color: #fff;
  border-radius: 2px;
  height: 32px;
  display: inline-flex;
  align-items: center;
  margin-bottom: 15px;
`;

const Anchor = styled.div`
  position: absolute;
  top: -80px;
`;

const ButtonScrollTop = styled.div`
  background: ${({ theme }) => theme.red};
  width: 60px;
  height: 60px;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #fff;
  position: fixed;
  bottom: 20%;
  right: 120px;
  z-index: 1000;
  opacity: ${({ isActive }) => (isActive ? 0.8 : 0)};
  overflow: hidden;
  visibility: ${({ isActive }) => (isActive ? 'visible' : 'hidden')};
  transform: ${({ isActive }) =>
    isActive ? 'translateY(0)' : 'translateY(-20px)'};
  cursor: pointer;
  transition: all 0.3s ease;
  &:hover {
    opacity: 1;
  }

  ${media.desktop`
    right: 60px;
    bottom: 10%;
  `}
  ${media.phone`
    right: 20px;
    bottom: 5%;
  `}
`;
