import React from 'react';
import styled from 'styled-components';
import { toast } from 'react-toastify';
import { Formik, Field, Form } from 'formik';
import Textarea from 'components/UI/Textarea';

function InlineComment({ postDetailCommentRequest, id, setToggle, auth }) {
  return (
    <Content>
      <Formik
        validate={values => {
          const errors = {};

          if (values.text.trim().length < 1) {
            errors.text = 'Текст больше быть больше 1 символов';
          }

          return errors;
        }}
        initialValues={{ text: '' }}
        onSubmit={values => {
          const params = {
            reply_id: id,
            ...values,
          };

          if (!auth) {
            return toast.error(
              'Оставить лайк или комментарий могут только авторизованные пользователи ⚠️',
            );
          }

          postDetailCommentRequest(params);
          setToggle(null);
        }}
      >
        {({ errors, isSubmitting, ...props }) => (
          <StyledForm>
            <Field
              placeholder="Ваш комментарий"
              name="text"
              component={Textarea}
            />
            <Button
              type="submit"
              disabled={!!Object.keys(errors).length || isSubmitting}
            >
              Отправить
            </Button>
          </StyledForm>
        )}
      </Formik>
    </Content>
  );
}

export default InlineComment;

const Content = styled.div`
  padding: 20px 0;
`;

const StyledForm = styled(Form)`
  display: grid;
  grid-gap: 16px 0;
  textarea {
    height: 140px;
    border: 1px solid rgba(0, 0, 0, 0.15);
    padding: 20px;
    width: 100%;
    font-size: 16px;
    resize: none;
    &:focus {
      outline: none;
    }
  }
`;

const Button = styled.button`
  width: 100%;
  cursor: pointer;
  border: none;
  margin-top: 20px;
  padding: 0 20px;
  height: 52px;
  color: #fff;
  font-size: 16px;
  background: ${({ theme }) => theme.red};
`;
