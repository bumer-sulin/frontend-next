import { useState, useEffect } from 'react';

const useScrollTop = ({ scrollRef }) => {
  const [scrollTop, setScrollTop] = useState(false);

  useEffect(() => {
    window.addEventListener('scroll', handleScrollTopArrow);

    return () => window.removeEventListener('scroll', handleScrollTopArrow);
  }, []);

  const handleScrollTopArrow = () => {
    const box =
      !!scrollRef &&
      !!scrollRef.current &&
      scrollRef.current.getBoundingClientRect();
    const { body } = document;
    const bodyReact = body.getBoundingClientRect();
    const docEl = document.documentElement;

    const scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;

    const clientTop = docEl.clientTop || body.clientTop || 0;

    const top = box.top + scrollTop - clientTop;

    if (
      scrollTop >= top + 400 &&
      document.body.scrollHeight - scrollTop - bodyReact.height >=
        bodyReact.height
    ) {
      return setScrollTop(true);
    }

    return setScrollTop(false);
  };

  return { scrollTop };
};

export default useScrollTop;
