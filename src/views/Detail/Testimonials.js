import React, { useState } from 'react';
import styled from 'styled-components';
import { IoIosPerson, IoMdHeartDislike, IoMdHeart } from 'react-icons/io';
import { ResetDefaultButton, media } from 'styles';
import { toast } from 'react-toastify';
import { Formik, Field, Form } from 'formik';
import Textarea from 'components/UI/Textarea';
import ReplayItems from './ReplayItems';
import InlineComment from './InlineComment';

function Testimonials({
  auth,
  article,
  comments,
  dislikeDetailCommentRequest,
  likeDetailCommentRequest,
  postDetailCommentRequest,
}) {
  const [toggleComment, setToggle] = useState('');

  function hasPermission() {
    if (!auth) {
      toast.error(
        'Оставить лайк или комментарий могут только авторизованные пользователи ⚠️',
      );
      return false;
    }

    return true;
  }

  function handleComment(id) {
    return (
      hasPermission() &&
      setToggle(prevComment => (prevComment === id ? '' : id))
    );
  }

  function handleLike(id) {
    const params = {
      article_id: article.id,
      comment_id: id,
    };

    return hasPermission() && likeDetailCommentRequest(params);
  }

  function handleDislike(id) {
    const params = {
      article_id: article.id,
      comment_id: id,
    };

    return hasPermission() && dislikeDetailCommentRequest(params);
  }

  return (
    <Content>
      <Header>
        <h3>
          {comments.length > 0
            ? `Комментариев (${comments.length})`
            : 'Нет комментариев'}
        </h3>
      </Header>

      {comments.length > 0 && (
        <Comments>
          {comments.map(comment => {
            const { author, text, created_at, children, id } = comment;

            return (
              <React.Fragment key={id}>
                <CommentItem isOpen>
                  <Avatar
                    style={{ backgroundImage: `url(${comment.image})` || '' }}
                  >
                    {!comment.image && <IoIosPerson size={34} />}
                  </Avatar>
                  <div>
                    <CommentHeader>
                      <Name>
                        <h4>{author}</h4>
                        <span>{created_at}</span>
                      </Name>
                    </CommentHeader>
                    <p>{text}</p>
                    <ReplayItems
                      {...comment}
                      handleDislike={handleDislike}
                      handleLike={handleLike}
                      handleComment={handleComment}
                      postDetailCommentRequest={postDetailCommentRequest}
                      hasPermission={hasPermission}
                    />

                    {toggleComment === id && (
                      <InlineComment
                        postDetailCommentRequest={postDetailCommentRequest}
                        id={id}
                        setToggle={setToggle}
                        auth={auth}
                      />
                    )}
                  </div>
                </CommentItem>
                {children.map(child => (
                  <CommentItem isReplay key={child.id}>
                    <Avatar
                      style={{ backgroundImage: `url(${child.image})` || '' }}
                    >
                      {!comment.image && <IoIosPerson size={34} />}
                    </Avatar>
                    <div>
                      <CommentHeader>
                        <Name>
                          <h4>{child.author}</h4>
                          <span>{child.created_at}</span>
                        </Name>
                      </CommentHeader>
                      <p>{child.text}</p>
                      <ReplayItems
                        {...child}
                        handleDislike={handleDislike}
                        handleLike={handleLike}
                        handleComment={handleComment}
                        hasPermission={hasPermission}
                      />
                      {toggleComment === child.id && (
                        <InlineComment
                          postDetailCommentRequest={postDetailCommentRequest}
                          id={child.id}
                          setToggle={setToggle}
                          auth={auth}
                        />
                      )}
                    </div>
                  </CommentItem>
                ))}
              </React.Fragment>
            );
          })}
        </Comments>
      )}

      <CommentsForm>
        <h3>Оставить Комментарий</h3>
        <Formik
          validate={values => {
            const errors = {};

            if (values.text.trim().length < 1) {
              errors.text = 'Текст больше быть больше 1 символов';
            }

            return errors;
          }}
          initialValues={{ text: '' }}
          onSubmit={(values, { resetForm }) => {
            const params = {
              ...values,
            };

            return (
              hasPermission() && postDetailCommentRequest(params) && resetForm()
            );
          }}
        >
          {({ errors, isSubmitting, ...props }) => (
            <StyledForm>
              <Field
                placeholder="Поделитесь своими мыслями..."
                name="text"
                component={Textarea}
              />
              <Button
                type="submit"
                disabled={!!Object.keys(errors).length || isSubmitting}
              >
                Отправить
              </Button>
            </StyledForm>
          )}
        </Formik>
      </CommentsForm>
    </Content>
  );
}

export default Testimonials;

const Content = styled.div`
  margin-bottom: 80px;
`;

const Comments = styled.div`
  margin-bottom: 60px;
`;

const Header = styled.div`
  display: grid;
  align-items: center;
  justify-content: space-between;
  grid-template-columns: 1fr auto;
  width: 100%;
  margin: 40px 0 60px;
  h3 {
    margin: 0 !important;
  }

  ${media.phone`
    grid-template-columns: 1fr;
    grid-gap: 24px 0;
  `}
`;

const CommentItem = styled.div`
  padding: ${({ isReplay }) =>
    isReplay ? '12px 24px 12px 64px' : '12px 24px'};
  display: grid;
  grid-template-columns: 60px auto;
  width: 100%;
  grid-gap: 0 16px;
  position: relative;
  transition: all 0.2s ease;
  p {
    margin: 0 !important;
    font-size: 16px !important;
    line-height: 24px !important;
  }
  &:hover {
    background: rgba(0, 0, 0, 0.015);
  }
  ${media.phone`
    padding: ${({ isReplay }) =>
      isReplay ? '25px 20px 25px 20px' : '25px 0px'};
    grid-template-columns: 1fr;
    grid-gap: 24px 0;
    p {
      font-size: 14px !important;
    }
  `}
`;

const CommentHeader = styled.div`
  display: grid;
  grid-auto-flow: column;
  justify-content: space-between;
  margin-bottom: 8px;
  align-items: flex-start;
  ${media.phone`
    margin-bottom: 20px;
  `}
`;

const Avatar = styled.div`
  width: 60px;
  height: 60px;
  /* background-color: rgba(0, 0, 0, 0.1); */
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  svg {
    color: rgb(119, 128, 140);
  }

  ${media.phone`
    display: none;
  `}
`;

const Name = styled.div`
  h4 {
    font-size: 14px;
    color: #222;
    margin-bottom: 0px;
    line-height: 21px;
    font-weight: 500;
  }
  span {
    font-size: 14px;
    font-weight: 400;
    color: #777;
  }
`;

const CommentsForm = styled.div`
  textarea {
    margin-top: 20px;
    height: 200px;
    border: 1px solid rgba(0, 0, 0, 0.15);
    padding: 20px;
    width: 100%;
    font-size: 16px;
    margin-bottom: 20px;
    resize: none;
    &:focus {
      outline: none;
    }
  }
  button {
    ${ResetDefaultButton};
    width: 100%;
    cursor: pointer;
    border: none;
    padding: 0 20px;
    height: 60px;
    color: #fff;
    font-size: 16px;
    background: ${({ theme }) => theme.red};
  }
`;

const StyledForm = styled(Form)`
  display: grid;
  grid-gap: 16px 0;
  textarea {
    height: 240px;
    border: 1px solid rgba(0, 0, 0, 0.15);
    padding: 20px;
    width: 100%;
    font-size: 16px;
    resize: none;
    &:focus {
      outline: none;
    }
  }
`;

const Button = styled.button`
  width: 100%;
  cursor: pointer;
  border: none;
  padding: 0 20px;
  height: 52px;
  color: #fff;
  font-size: 16px;
  background: ${({ theme }) => theme.red};
`;

const Error = styled.span`
  color: rgba(250, 134, 134);
  margin-top: 10px 0;
  display: block;
  font-size: 14px;
`;
