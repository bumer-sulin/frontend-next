import React, { useEffect } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { selectors, actions } from 'redux/ducks/home.duck';
import Section from 'components/Section';
import Article from 'components/Article';
import Slider from 'components/Slider';
import { LoadingDefault } from 'components/UI/Loading';
import { media } from 'styles';

function Home({
  slides,
  sections,
  photoSlides,
  loading,
  fetchMainSectionsRequest,
  fetchSlidesRequest,
  fetchPhotoSlidesRequest,
}) {
  useEffect(() => {
    fetchMainSectionsRequest();
    fetchSlidesRequest();
    fetchPhotoSlidesRequest();
  }, []);

  return loading && sections.length > 0 ? (
    <LoadingDefault />
  ) : (
    <div>
      {slides.length > 0 && (
        <Section title="Главные новости" noMore>
          <Slider items={slides} />
        </Section>
      )}

      {sections.slice(0, 2).length > 0 &&
        sections.slice(0, 2).map(({ title, id, articles }) => (
          <Section title={title} key={id} categoryId={id}>
            <Row>
              {articles.length > 0 &&
                articles.map(article => (
                  <Article key={article.id} {...article} />
                ))}
            </Row>
          </Section>
        ))}

      {photoSlides.length > 0 && (
        <Section title="Фото" noMore>
          <Slider items={photoSlides} gallery />
        </Section>
      )}

      {sections.slice(2).length > 0 &&
        sections.slice(2).map(({ title, id, articles }) => (
          <Section title={title} key={id} categoryId={id}>
            <Row>
              {articles.length > 0 &&
                articles.map(article => (
                  <Article key={article.id} {...article} />
                ))}
            </Row>
          </Section>
        ))}
    </div>
  );
}

Home.propTypes = {
  slides: PropTypes.instanceOf(Array).isRequired,
  photoSlides: PropTypes.instanceOf(Array).isRequired,
  sections: PropTypes.instanceOf(Array).isRequired,
  loading: PropTypes.bool.isRequired,
  fetchMainSectionsRequest: PropTypes.func.isRequired,
  fetchSlidesRequest: PropTypes.func.isRequired,
  fetchPhotoSlidesRequest: PropTypes.func.isRequired,
};

export default connect(
  state => ({
    loading: selectors.getLoading(state),
    slides: selectors.getSlides(state),
    photoSlides: selectors.getPhotoSlides(state),
    sections: selectors.getSections(state),
  }),
  { ...actions },
)(Home);

const Row = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 30px;
  ${media.phone`
    grid-template-columns: 1fr;
  `}
`;
