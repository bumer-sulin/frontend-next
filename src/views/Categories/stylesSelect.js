export default {
  container: () => ({
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
  }),
  indicatorsContainer: provided => ({
    ...provided,
    alignItems: 'start',
  }),
  indicatorSeparator: () => ({
    borderLeft: 'none',
  }),
  option: (provided, state) => ({
    ...provided,
    color: state.isSelected ? '#fff' : '#000',
    background: state.isSelected ? '#f13d37' : '#fff',
    cursor: state.isSelected ? 'default' : 'pointer',
    padding: '13px 15px',
    ':hover': {
      background: state.isSelected ? '#f13d37' : 'rgba(0, 0, 0, 0.043)',
    },
  }),
  control: () => ({
    paddingTop: '14px',
    width: '100%',
    display: 'flex',
    alignItems: 'stretch',
    height: '60px',
  }),
  valueContainer: provided => ({
    ...provided,
    paddingLeft: '12px',
  }),
  singleValue: (provided, state) => {
    const opacity = state.isDisabled ? 0.5 : 1;
    const transition = 'opacity 300ms';

    return { ...provided, opacity, transition, marginLeft: '3px' };
  },
  menu: provided => ({
    ...provided,
    borderRadius: '0',
    padding: '0',
  }),
  menuList: provided => ({
    ...provided,
    borderRadius: '0',
    padding: '0',
  }),
  menuPortal: provided => ({
    ...provided,
    border: '1px solid rgba(0,0,0,0.1)',
    borderRadius: '0',
    padding: '0',
  }),
  multiValue: styles =>
    // const color = chroma(data.color);
    ({
      ...styles,
      backgroundColor: '#f13d37',
    }),
  multiValueLabel: styles => ({
    ...styles,
    color: '#fff',
  }),
  multiValueRemove: styles => ({
    ...styles,
    color: '#fff',
    ':hover': {
      opacity: 0.9,
    },
  }),
};
