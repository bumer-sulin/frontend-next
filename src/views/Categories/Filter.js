import React, { useMemo } from 'react';
import { useRouter } from 'next/router';
import DatePicker, { registerLocale } from 'react-datepicker';
import moment from 'moment';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { selectors } from 'redux/ducks/main.duck';
import { actions as actionsCategory } from 'redux/ducks/category.duck';
import { IoIosSearch } from 'react-icons/io';
import Select from 'react-select';
import { Form, Formik, Field } from 'formik';
import Section from 'components/Section';
import { media } from 'styles';
import { LoadingDefault } from 'components/UI/Loading';
import urlParams from 'lib/helpers/searchCategory';
import MaskedTextInput from 'react-text-mask';
import stylesSelect from './stylesSelect';
import 'moment/locale/ru';
import ru from 'date-fns/locale/ru';

moment.locale('ru');

registerLocale('ru', ru);

const Filter = ({
  categories,
  tags,
  categoryId,
  loading,
  searchText,
  handleScroll,
  tagsIds,
  urlDate,
}) => {
  const router = useRouter();
  const initialCategories = !!categories && [
    { title: 'Все Новости', id: 'all' },
    ...categories,
  ];

  const initTag = useMemo(() => {
    if (!tagsIds) {
      return [];
    }

    return tags.filter(tag => tagsIds.includes(String(tag.id)));
  }, [tagsIds]);

  const defaultCategories =
    categoryId && categories.length > 0
      ? categories.find(e => e.id === Number(categoryId))
      : initialCategories[0];

  const upgradeInSelect = data =>
    data.map(({ id, title }) => ({ value: id, label: title }));

  const checkDate = date => {
    if (
      !!Date.parse(moment(date, 'DD-MM-YYYY').toDate()) === false ||
      Date.parse(moment(date, 'DD-MM-YYYY').toDate()) >= Date.now()
    ) {
      return null;
    }

    return moment(date, 'DD-MM-YYYY').toDate();
  };

  return loading && categories.length > 0 ? (
    <LoadingDefault />
  ) : (
    !!categories && (
      <Section title="Фильтр" noMore>
        <Formik
          // enableReinitialize
          initialValues={{
            search: searchText || '',
            date: checkDate(urlDate),
            category: {
              label: defaultCategories.title,
              value: defaultCategories.id,
            },
            tags: upgradeInSelect(initTag || []),
          }}
          onSubmit={async ({ search, category, tags, date }) => {
            const settingTags = tags.map(e => e.value);
            const newDate = (date && moment(date).format('DD-MM-YYYY')) || '';

            if (category.value === 'all') {
              await router.push({
                pathname: '/search',
                query: {
                  search,
                  tags: settingTags,
                  date: newDate,
                },
              });
              await handleScroll();
            } else {
              await router.push({
                pathname: '/search',
                query: {
                  search,
                  category_id: category.value,
                  tags: settingTags,
                  date: newDate,
                },
              });
              await handleScroll();
            }
          }}
        >
          {({ values, setFieldValue }) => (
            <StyledForm>
              <SearchContainer>
                <Search>
                  <IoIosSearch size={24} />
                  <Field name="search" placeholder="Поиск" />
                </Search>
                <StyledDateInput>
                  <Label>Дата с</Label>
                  <Field
                    name="date"
                    render={({ field }) => (
                      <DatePicker
                        selected={field.value}
                        placeholderText="xx-xx-xxxx"
                        isClearable
                        customInput={
                          <MaskedTextInput
                            type="text"
                            autoComplete="off"
                            mask={[
                              /\d/,
                              /\d/,
                              '-',
                              /\d/,
                              /\d/,
                              '-',
                              /\d/,
                              /\d/,
                              /\d/,
                              /\d/,
                            ]}
                          />
                        }
                        locale="ru"
                        maxDate={new Date()}
                        dateFormat="dd-MM-yyyy"
                        onChange={date => {
                          return setFieldValue('date', date);
                        }}
                      />
                    )}
                  />
                </StyledDateInput>
              </SearchContainer>
              <CategoriesFilter>
                <StyledSelect>
                  <Label>Категории</Label>
                  <Select
                    className="basic-single"
                    classNamePrefix="select"
                    value={values.category}
                    name="category"
                    options={upgradeInSelect(initialCategories)}
                    styles={stylesSelect}
                    onChange={optionSelect =>
                      setFieldValue('category', optionSelect)
                    }
                  />
                </StyledSelect>
                <StyledSelect>
                  <Label>Теги</Label>
                  <Select
                    value={values.tags}
                    closeMenuOnSelect={!(values.tags.length < 2)}
                    name="tags"
                    onChange={optionTags => {
                      if (!optionTags) {
                        return setFieldValue('tags', []);
                      }

                      if (optionTags.length < 4) {
                        setFieldValue('tags', optionTags);
                      }
                    }}
                    placeholder="Выберите тег"
                    isMulti
                    options={upgradeInSelect(tags)}
                    styles={stylesSelect}
                  />
                </StyledSelect>
              </CategoriesFilter>
              <button type="submit">Применить</button>
            </StyledForm>
          )}
        </Formik>
      </Section>
    )
  );
};

Filter.defaultProps = {
  searchText: '',
  categoryId: null,
};

Filter.propTypes = {
  categories: PropTypes.instanceOf(Array).isRequired,
  tags: PropTypes.instanceOf(Array).isRequired,
  fetchDetailsCategoryRequest: PropTypes.func.isRequired,
  categoryId: PropTypes.string,
  loading: PropTypes.bool.isRequired,
  history: PropTypes.shape({}).isRequired,
  searchText: PropTypes.string,
};

export default connect(
  state => ({
    tags: selectors.getTags(state),
    categories: selectors.getCategories(state),
    loading: selectors.getLoading(state),
  }),
  { ...actionsCategory },
)(Filter);

const StyledForm = styled(Form)`
  input {
    border: 1px solid rgba(0, 0, 0, 0.15);
    padding-left: 20px;
    width: 100%;
    font-size: 16px;

    &:focus {
      outline: none;
    }
  }

  & > button {
    width: 100%;
    cursor: pointer;
    border: none;
    padding: 0 20px;
    height: 60px;
    color: #fff;
    font-size: 16px;
    background: ${({ theme }) => theme.red};
  }
`;

const SearchContainer = styled.div`
  display: grid;
  grid-template-columns: auto 140px;
  grid-gap: 0 14px;
  margin-bottom: 20px;
  ${media.phone`
    grid-template-columns: 1fr;
    grid-gap: 16px 0;
  `}
`;

const CategoriesFilter = styled.div`
  display: grid;
  grid-template-columns: 230px auto;
  grid-gap: 0 14px;
  margin-bottom: 20px;
  ${media.phone`
    grid-template-columns: 1fr;
    grid-gap: 16px 0;
  `}
`;

const StyledDateInput = styled.div`
  position: relative;
  border: 1px solid rgba(0, 0, 0, 0.1);
  width: 100%;
  height: 60px;

  .react-datepicker__close-icon {
    transition: all 0.3s ease;

    &:after {
      color: hsl(0,0%,80%);
      background: transparent;
      font-size: 18px;
      height: 20px;
      width: 20px;
    }

    &:hover {
      &:after {
        color: hsl(0,0%,60%);
      }
    }
  }

  .react-datepicker__navigation.react-datepicker__navigation--previous {
    border-right-color: #fff;
  }

  .react-datepicker__navigation.react-datepicker__navigation--next {
    border-left-color: #fff;
  }

  .react-datepicker__current-month {
    color: #fff;
  }
  
  .react-datepicker {
    box-shadow: 0 4px 14px rgba(0, 0, 0, 0.13);
    border-radius: 0 !important;
    border: none !important;
  }

  .react-datepicker__day--selected, .react-datepicker__day--in-selecting-range, .react-datepicker__day--in-range {
    background ${({ theme }) => theme.red};
    &:hover {
      background ${({ theme }) => theme.red};
      opacity: 0.8;
    }
  }

  .react-datepicker__header {
    background ${({ theme }) => theme.red};
    border-bottom: none;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
  }

  .react-datepicker__day--keyboard-selected {
    background-color: ${({ theme }) => theme.red};
  }

  .react-datepicker-popper[data-placement^="bottom"] .react-datepicker__triangle, .react-datepicker-popper[data-placement^="bottom"] .react-datepicker__triangle{
    &:before  {
      border-top-color: transparent !important;
      border-bottom-color: transparent !important;
    }
  }

  .react-datepicker-popper[data-placement^="top"] .react-datepicker__triangle::before, .react-datepicker__year-read-view--down-arrow::before, .react-datepicker__month-read-view--down-arrow::before, .react-datepicker__month-year-read-view--down-arrow::before {
    border-top-color: transparent !important;
      border-bottom-color: transparent !important;
  }

  .react-datepicker__triangle {
    border-bottom-color: ${({ theme }) => theme.red} !important;
  }

  .react-datepicker__day-names {
    margin-top: 20px;
    .react-datepicker__day-name {
      color: #fff;
    }
  }


  & > label {
    padding-left: 15px;
    font-size: 12px;
    margin-top: 3px;
    display: block;
    color: rgba(0, 0, 0, 0.4);
  }
  input {
    border: none;
    background: transparent;
    padding-top: 3px;
    padding-left: 13px;
    width: 100%;
    height: 100%;
  }
`;

const StyledSelect = styled.div`
  position: relative;
  border: 1px solid rgba(0, 0, 0, 0.1);
  width: 100%;
  height: 60px;

  & > label {
    padding-left: 15px;
    font-size: 12px;
    margin-top: 3px;
    display: block;
    color: rgba(0, 0, 0, 0.4);
  }
`;

const Search = styled.div`
  position: relative;
  width: 100%;

  svg {
    position: absolute;
    top: 50%;
    left: 0;
    margin: 0 15px;
    transform: translateY(-50%);
    z-index: -1;
    color: rgba(0, 0, 0, 0.43);
  }

  input {
    height: 60px;
    padding-left: 48px;
    background: transparent;
  }
`;

const Label = styled.label``;
