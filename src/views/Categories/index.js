import React, { useState, useEffect, useRef } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { LoadingDefault } from 'components/UI/Loading';
import { selectors, actions } from 'redux/ducks/category.duck';
import styled from 'styled-components';
import Filter from './Filter';
import List from './List';

function Categories({
  loading,
  items,
  fetchDetailsCategoryRequest,
  pagination,
}) {
  const scroll = useRef(null);
  const scrollItems = useRef(null);
  const router = useRouter();

  const [searchParams, setSearchParams] = useState(router.query);

  useEffect(() => {
    const params = Object.keys(router.query);

    if (
      params.find(e => e === 'category_id') ||
      params.find(e => e === 'search') ||
      params.find(e => e === 'tags[]')
    ) {
      scroll.current.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
        inline: 'nearest',
      });
    }
  }, [router.query]);

  useEffect(() => {
    setSearchParams(router.query);
    fetchDetailsCategoryRequest(router.query);
  }, [router.query]);

  // useEffect(() => {
  //   fetchDetailsCategoryRequest(router.query);
  // }, []);

  function handleScroll() {
    scrollItems.current.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
      inline: 'nearest',
    });
  }

  return (
    <Content ref={scroll}>
      <Head>
        <title>{`Поиск по категориям ${searchParams.search ||
          'Все Новости'}`}</title>
        <meta name="description" content="Nested component" />
      </Head>
      <div>
        <Filter
          searchText={searchParams.search}
          categoryId={searchParams.category_id}
          tagsIds={searchParams.tags}
          urlDate={searchParams.date}
          // history={history}
          handleScroll={handleScroll}
        />
        <div ref={scrollItems}>
          {loading ? (
            <LoadingDefault />
          ) : (
            <List
              items={items}
              categoryId={searchParams.category_id}
              // location={location}
              // history={history}
              pagination={pagination}
              fetchDetailsCategoryRequest={fetchDetailsCategoryRequest}
              handleScroll={handleScroll}
            />
          )}
        </div>
      </div>
    </Content>
  );
}

Categories.propTypes = {
  items: PropTypes.instanceOf(Array).isRequired,
  fetchDetailsCategoryRequest: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  history: PropTypes.shape({}).isRequired,
  location: PropTypes.shape({}).isRequired,
};

export default connect(
  state => ({
    loading: selectors.getLoading(state),
    items: selectors.getItems(state),
    pagination: selectors.getPagination(state),
  }),
  { ...actions },
)(Categories);

const Content = styled.div``;
