import React from 'react';
import styled from 'styled-components';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { selectors } from 'redux/ducks/main.duck';
import { IoIosArrowBack, IoIosArrowForward } from 'react-icons/io';
import InlineArticle from 'components/InlineArticle';
import { ResetDefaultButton } from 'styles';
import Section from 'components/Section';

function List({
  items,
  categoryId,
  categories,
  fetchDetailsCategoryRequest,
  pagination,
  handleScroll,
}) {
  const router = useRouter();
  const currentCategories = categories.find(e => e.id === Number(categoryId));
  const title = currentCategories ? currentCategories.title : 'Все Новости';

  function range(min, max, step = 1) {
    const arr = [];
    const totalSteps = Math.floor((max - min) / step);
    // eslint-disable-next-line no-plusplus
    for (let ii = 0; ii <= totalSteps; ii++) {
      arr.push(ii * step + min);
    }
    return arr;
  }

  function getPages(current) {
    const { total, limit } = pagination;

    const stepNeighbour = Math.floor(3 / 2);
    const maxTotal = Math.ceil(total / limit);
    const firstElemCenter = Math.max(
      current -
        Math.min(stepNeighbour, current - 1) +
        Math.min(0, maxTotal - stepNeighbour - current),
      1,
    );
    const lastElemCenter = Math.min(firstElemCenter + 3 - 1, maxTotal);
    const arrStart = range(1, Math.min(firstElemCenter - 1, 1));
    if (firstElemCenter - 1 > 1) {
      arrStart.push('«');
    }

    const arrCenter = range(firstElemCenter, lastElemCenter);

    const arrEnd = range(
      maxTotal,
      Math.max(lastElemCenter + 1, maxTotal - 1 + 1),
      -1,
    );
    if (lastElemCenter + 1 < maxTotal - 1 + 1) {
      arrEnd.push('»');
    }

    return arrStart.concat(arrCenter, arrEnd.reverse());
  }

  const prevPage = () => {
    const { page } = pagination;

    if (page - 1 !== 0) {
      handleScroll();
      return fetchDetailsCategoryRequest({ ...router.query, page: page - 1 });
    }

    return false;
  };

  const nextPage = () => {
    const { page, total, limit } = pagination;
    const maxTotal = Math.ceil(total / limit);

    if (page + 1 <= maxTotal) {
      handleScroll();
      return fetchDetailsCategoryRequest({ ...router.query, page: page + 1 });
    }

    return false;
  };

  function handlePagination(page) {
    handleScroll();
    fetchDetailsCategoryRequest({ ...router.query, page });
  }

  const maxTotalCount =
    pagination && Math.ceil(pagination.total / pagination.limit);

  return (
    <div>
      <Section title={title} noMore>
        {items.length > 0 ? (
          items.map(item => {
            const { id } = item;

            return <InlineArticle {...item} key={id} />;
          })
        ) : (
          <NotItems>Нет Новостей</NotItems>
        )}

        {maxTotalCount > 1 && (
          <Pagination>
            <div>
              <button
                type="button"
                onClick={prevPage}
                disabled={pagination.page === 1}
              >
                <IoIosArrowBack size={24} />
              </button>
              {getPages(Number(pagination.page)).map((e, index) => (
                <button
                  key={index}
                  type="button"
                  className={e === pagination.page ? 'is-active' : ''}
                  disabled={e === pagination.page}
                  onClick={() =>
                    handlePagination(
                      // eslint-disable-next-line no-nested-ternary
                      e === '«'
                        ? pagination.page - 3
                        : e === '»'
                        ? pagination.page + 3
                        : e,
                    )
                  }
                >
                  {e}
                </button>
              ))}
              <button
                type="button"
                onClick={nextPage}
                disabled={
                  pagination.page ===
                  Math.ceil(pagination.total / pagination.limit)
                }
              >
                <IoIosArrowForward size={24} />
              </button>
            </div>
          </Pagination>
        )}
      </Section>
    </div>
  );
}

List.propTypes = {
  items: PropTypes.instanceOf(Array).isRequired,
  categories: PropTypes.instanceOf(Array).isRequired,
  // categoryId: PropTypes.string,
};

export default connect(state => ({
  categories: selectors.getCategories(state),
}))(List);

const Pagination = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 60px;
  & > div {
    margin: 0;
    padding: 0;
    list-style-type: none;
    display: grid;
    grid-auto-flow: column;
    grid-gap: 0 8px;
  }

  button {
    ${ResetDefaultButton}
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 8px 14px;
    color: #222;
    font-weight: normal;
    &:first-of-type,
    &:last-of-type {
      padding: 8px 9px;
      background-color: none;
      &:disabled {
        opacity: 0.7;
      }
    }
  }

  span {
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 0 4px;
    font-weight: 500;
  }

  .is-active {
    background-color: ${({ theme }) => theme.red};
    color: #fff;
  }
`;

const NotItems = styled.h2`
  margin: 80px 0;
  text-align: center;
  color: rgba(0, 0, 0, 0.6);
  font-weight: 300;
  font-size: 32px;
`;
