import { css, createGlobalStyle } from 'styled-components';
import reset from 'styled-reset';
import { media } from './media';

export const GlobalStyle = createGlobalStyle`
  ${reset}

  * {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    border-radius: 0;
  }
  a {
    cursor: pointer;
  }

  textarea,
  input,
   {
    -webkit-appearance: none!important;
    border-radius: 0!important;
  }

  #root,
  body,
  html {
    height: 100%;
  }

  #wrapper {
    min-height: 100vh; position: relative;
  }

  #modal-root {
    position: relative;
    z-index: 1050;
  }

  body {
    margin: 0;
    padding: 0;
    font-family: 'Roboto', sans-serif;
    font-size: 16px;
    color: #222222;
    &.isOpen {
      @media (max-width: 600px) {
        #wrapper {
          overflow: hidden;
        }
        overflow: hidden;
      }
    }
  }

  * {
    box-sizing: border-box;
  }

  h1 {
    line-height: 38px;
    font-size: 34px;
    font-weight: 700;
  }

  h2 {
    line-height: 34px;
    font-size: 28px;
    font-weight: 700;
    color: #212631;
    margin: 0px 0px 15px;
    ${media.phone`
      font-size: 22px;
    `}
  }

  h3 {
    font-size: 23px;
    line-height: 30px;
    font-weight: 700;
    color: #212631;
    margin: 0px 0px 15px;
    ${media.phone`
      font-size: 19px;
    `}
  }

  h4 {
    font-size: 16px;
    line-height: 18px;
    font-weight: 700;
    color: #212631;
    margin: 0px 0px 15px;
  }


  p {
    line-height: 22px;
    font-weight: 300;
    margin: 0px 0px 20px;
  }


  button {
    font-weight: 400;
    font-family: 'Roboto', sans-serif;
    font-size: 16px;
    color: #222222;
    &:focus {
      outline: none;
    }
  }
  a {
    line-height: 22px;
    color: initial;
    text-decoration: none;
    transition: all 0.2s ease;
    &:hover {
      /* color: #0050ef; */
      opacity: 0.8;
    }
  }

  label {
    font-size: 16px;
    line-height: 22px;
  }

  img {
    max-width: 100%;
    height: auto;
  }

  button {
    padding-bottom: 0;
  }
  .toast-custom {
    &.Toastify__toast--success {
      background: #36b274;
    }
  }

`;

export const ResetDefaultButton = css`
  border: none;
  background: none;
  padding: 0;
  margin: 0;
  cursor: pointer;
`;

export const Container = css`
  max-width: 1170px;
  margin: 0 auto;
  padding: 0 15px;
`;
