import { createStore, applyMiddleware } from 'redux';
// import { createBrowserHistory } from 'history';
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import sagasManager from 'lib/sagasManager';
import { createWrapper } from 'next-redux-wrapper';
import rootReducer from './rootReducer';

// const sagaMiddleware = createSagaMiddleware();
const initialState = {};

// export const history = createBrowserHistory();
// export const store = createStore(
//   rootReducer(history),
//   initialState,
//   applyMiddleware(
//     sagaMiddleware,
//     thunk,
//     routerMiddleware(history),
//   ),
// );

// export default () => {
//   sagaMiddleware.run(sagasManager.getRootSaga());
//   store.sagaTask = sagaMiddleware.run(rootSaga)

//   return { store, history };
// };

export const makeStore = () => {
  const sagaMiddleware = createSagaMiddleware();
  // const store = createStore(rootReducer, bindMiddleware([sagaMiddleware]))
  const store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(sagaMiddleware, thunk),
  );

  store.sagaTask = sagaMiddleware.run(sagasManager.getRootSaga());

  return store;
};

export const wrapper = createWrapper(makeStore, { debug: true });
