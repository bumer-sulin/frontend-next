import { handleActions, createActions } from 'redux-actions';
import { createSelector } from 'reselect';
import sagasManager from 'lib/sagasManager';
import { toast } from 'react-toastify';
import { all, call, put, select, takeEvery } from 'redux-saga/effects';
import { removeHash, setHash } from 'lib/helpers/token';
import api from 'lib/api';
import { actions as actionsAuth } from './auth.duck';
import { actions as actionsModal } from './modals.duck';

const initialState = {
  profile: null,
  error: false,
  loading: false,
};

const actions = createActions(
  'RESET_PROFILE',

  'FETCH_PROFILE_REQUEST',
  'FETCH_PROFILE_SUCCESS',
  'FETCH_PROFILE_FAILURE',

  'FETCH_PROFILE_STARTUPS_REQUEST',
  'FETCH_PROFILE_STARTUPS_SUCCESS',
  'FETCH_PROFILE_STARTUPS_FAILURE',

  'FETCH_PROFILE_TRACKINGS_REQUEST',
  'FETCH_PROFILE_TRACKINGS_SUCCESS',
  'FETCH_PROFILE_TRACKINGS_FAILURE',

  'UPDATE_PROFILE_REQUEST',
  'UPDATE_PROFILE_SUCCESS',
  'UPDATE_PROFILE_FAILURE',

  'UPDATE_PASS_PROFILE_REQUEST',
  'UPDATE_PASS_PROFILE_SUCCESS',
  'UPDATE_PASS_PROFILE_FAILURE',
);

const reducer = handleActions(
  {
    [actions.resetProfile]: () => ({
      ...initialState,
    }),

    [actions.fetchProfileRequest]: state => ({
      ...state,
      loading: true,
      error: false,
    }),
    [actions.fetchProfileSuccess]: (state, { payload }) => ({
      ...state,
      profile: payload,
      loading: false,
    }),
    [actions.fetchProfileFailure]: state => ({
      ...state,
      loading: false,
      error: true,
    }),

    [actions.updateProfileRequest]: state => ({
      ...state,
      loading: true,
      error: false,
    }),
    [actions.updateProfileSuccess]: state => ({
      ...state,
      loading: false,
    }),
    [actions.updateProfileFailure]: state => ({
      ...state,
      loading: false,
      error: true,
    }),

    [actions.updatePassProfileRequest]: state => ({
      ...state,
      loading: true,
      error: false,
    }),
    [actions.updatePassProfileSuccess]: state => ({
      ...state,
      loading: false,
    }),
    [actions.updatePassProfileFailure]: state => ({
      ...state,
      loading: false,
      error: true,
    }),
  },
  initialState,
);

const effects = {
  fetchProfile: function* fetchProfileSaga({ payload }) {
    try {
      const profile = yield select(state => state.user.profile);
      const data = yield call(api.getProfile, payload);

      yield put(actions.fetchProfileSuccess(data));
      yield put(actionsAuth.authRefreshToken(payload));
      setHash(payload);
      // if (!profile) {
      //   toast.success(`Вы вошли под ${data.email}`, { autoClose: 1500 });
      // }
    } catch (error) {
      yield put(actions.fetchProfileFailure(error));
      removeHash();
    }
  },
  updateProfile: function* updateProfileSaga({ payload }) {
    try {
      const hash = yield select(state => state.auth.hash);
      const id = yield select(state => state.user.profile.id);
      const data = yield call(api.updateProfile, { ...payload, id, hash });

      yield put(actions.updateProfileSuccess());
      yield put(actions.fetchProfileRequest(data.hash));
      yield put(actionsModal.closeModal());
      toast.success(data.message);
      setHash(data.hash);
    } catch (error) {
      yield put(actions.updateProfileFailure(error));
      toast.error(error.data.message);
    }
  },
  updatePassProfile: function* updatePassProfileSaga({ payload }) {
    try {
      const {
        data: { meta, data },
      } = yield call(api.updatePassword, payload);

      if (meta.success) {
        yield put(actions.updatePassProfileSuccess(data));
        yield put(actions.fetchProfileRequest());
      }
    } catch (error) {
      yield put(actions.updatePassProfileFailure(error));
    }
  },
};

const getState = state => state.user;
const cs = cb => createSelector([getState], cb);

const selectors = {
  getProfile: cs(s => s.profile),
  getErrors: cs(s => s.error),
  getLoading: cs(s => s.loading),
};

sagasManager.addSagaToRoot(function* watcher() {
  yield all([
    takeEvery(actions.fetchProfileRequest, effects.fetchProfile),
    takeEvery(actions.updateProfileRequest, effects.updateProfile),
    takeEvery(actions.updatePassProfileRequest, effects.updatePassProfile),
  ]);
});

export { initialState as state, reducer, actions, selectors, effects };
