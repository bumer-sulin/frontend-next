import { handleActions, createActions } from 'redux-actions';
import { createSelector } from 'reselect';
import api from 'lib/api';
import { setHash, removeHash } from 'lib/helpers/token';
import { toast } from 'react-toastify';
import { actions as actionsUser } from './user.duck';
import { actions as actionsModal } from './modals.duck';
// import { push } from 'connected-react-router';

const initialState = {
  hash: null,
  error: false,
  loading: false,
};

const actions = createActions(
  'AUTH_LOGIN_REQUEST',
  'AUTH_LOGIN_SUCCESS',
  'AUTH_LOGIN_FAILURE',

  'AUTH_SIGNUP_REQUEST',
  'AUTH_SIGNUP_SUCCESS',
  'AUTH_SIGNUP_FAILURE',

  'AUTH_LOGOUT_REQUEST',
  'AUTH_LOGOUT_SUCCESS',
  'AUTH_LOGOUT_FAILURE',

  'AUTH_REFRESH_TOKEN',

  'GET_PASS_SUCCESS',
  'GET_PASS_FAILURE',
  'GET_PASS_REQUEST',

  'RESET_PASS_SUCCESS',
  'RESET_PASS_FAILURE',
  'RESET_PASS_REQUEST',
);

const reducer = handleActions(
  {
    [actions.authLoginRequest]: state => ({
      ...state,
      loading: true,
      error: false,
    }),
    [actions.authLoginSuccess]: (state, { payload }) => ({
      ...state,
      hash: payload,
      loading: false,
    }),
    [actions.authLoginFailure]: state => ({
      ...state,
      loading: false,
      error: true,
    }),

    [actions.authSignupRequest]: state => ({
      ...state,
      loading: true,
      error: false,
    }),
    [actions.authSignupSuccess]: (state, { payload }) => ({
      ...state,
      hash: payload,
      loading: false,
    }),
    [actions.authSignupFailure]: state => ({
      ...state,
      loading: false,
      error: true,
    }),

    [actions.resetPassRequest]: state => ({
      ...state,
      loading: true,
      error: false,
    }),
    [actions.resetPassSuccess]: state => ({
      ...state,
      loading: false,
    }),
    [actions.resetPassFailure]: state => ({
      ...state,
      loading: false,
      error: true,
    }),

    [actions.authLogoutRequest]: state => ({
      ...state,
      loading: true,
    }),
    [actions.authLogoutSuccess]: () => ({
      ...initialState,
    }),
    [actions.authLogoutFailure]: state => ({
      ...state,
      loading: false,
      error: true,
    }),

    [actions.getPassRequest]: state => ({
      ...state,
      loading: true,
    }),
    [actions.getPassSuccess]: state => ({
      ...state,
    }),
    [actions.getPassFailure]: state => ({
      ...state,
      loading: false,
      error: true,
    }),

    [actions.authRefreshToken]: (state, { payload }) => ({
      ...state,
      hash: payload,
    }),
  },
  initialState,
);

const effects = {
  authLogin: (payload, setErrors, setSubmitting) => async dispatch => {
    try {
      const { hash } = await api.authLogin(payload);

      Promise.all([
        dispatch(actions.authLoginSuccess(hash)),
        setSubmitting(false),
        setHash(hash),
        dispatch(actionsModal.closeModal()),
        dispatch(actionsUser.fetchProfileRequest(hash)),
      ]);
    } catch (error) {
      const { data } = error;
      setErrors({ email: data.errors.email, password: data.errors.email });
      setSubmitting(false);
      toast.error(data.errors.email);
    }
  },
  authSignup: (payload, setErrors, setSubmitting) => async dispatch => {
    try {
      const { hash } = await api.authSingup(payload);

      Promise.all([
        dispatch(actions.authLoginSuccess(hash)),
        setHash(hash),
        dispatch(actionsModal.closeModal()),
        dispatch(actionsUser.fetchProfileRequest(hash)),
      ]);
    } catch (error) {
      const {
        data: { errors },
      } = error;

      setSubmitting(false);
      setErrors({ ...errors });
    }
  },
  authLogout: () => dispatch => {
    dispatch(actions.authLogoutSuccess());
    dispatch(actionsUser.resetProfile());
    removeHash();
    toast.error('Вы вышли из профиля', { autoClose: 1500 });
  },
  getPassword: (payload, setErrors, setSubmitting) => async dispatch => {
    try {
      await dispatch(actions.getPassRequest());
      const { message } = await api.authGetPass(payload);

      await dispatch(actions.getPassSuccess());
      await dispatch(actionsModal.closeModal());
      toast.success(message);
      setSubmitting(false);
    } catch (error) {
      const {
        data: { errors },
      } = error;

      await dispatch(actions.getPassSuccess());
      await setErrors({ ...errors });
      await setSubmitting(false);
    }
  },
  resetPass: (payload, setErrors, setSubmitting) => async dispatch => {
    try {
      await dispatch(actions.resetPassRequest());
      const { hash, message } = await api.resetPass(payload);

      await dispatch(actions.resetPassSuccess());
      await dispatch(actionsModal.closeModal());
      await dispatch(actionsUser.fetchProfileRequest(hash));
      toast.success(message);
      setSubmitting(false);
      // await push('/');
    } catch (error) {
      const {
        data: { errors },
      } = error;

      await dispatch(actions.resetPassSuccess());
      await setErrors({ ...errors });
      await setSubmitting(false);
    }
  },
};

const getState = state => state.auth;
const cs = cb => createSelector([getState], cb);

const selectors = {
  getHash: cs(s => s.hash),
  getErrors: cs(s => s.error),
  getLoading: cs(s => s.loading),
};

export { initialState as state, reducer, actions, selectors, effects };
