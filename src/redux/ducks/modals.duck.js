import { createActions, handleActions } from 'redux-actions';
import { createSelector } from 'reselect';

const initialState = {
  type: null,
  open: false,
  args: null,
};

const actions = createActions('CLOSE_MODAL', 'OPEN_MODAL');

const reducer = handleActions(
  {
    [actions.openModal]: (state, { payload: { type, args } }) => ({
      ...state,
      open: true,
      type,
      args,
    }),
    [actions.closeModal]: () => ({ ...initialState }),
  },
  initialState,
);

const getState = state => state.modals;

const cs = cb => createSelector([getState], cb);

const selectors = {
  getType: cs(s => s.type),
  isOpen: cs(s => s.open),
  getArgs: cs(s => s.args),
};

export { initialState as state, reducer, actions, selectors };
