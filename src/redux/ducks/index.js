export { reducer as auth } from './auth.duck';
export { reducer as user } from './user.duck';
export { reducer as modals } from './modals.duck';
export { reducer as main } from './main.duck';
export { reducer as detailsCategory } from './category.duck';
export { reducer as home } from './home.duck';
export { reducer as detailArticle } from './article.duck';
export { reducer as promotions } from './promotions.duck';
