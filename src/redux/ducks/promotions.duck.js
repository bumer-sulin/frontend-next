import { handleActions, createActions } from 'redux-actions';
import { createSelector } from 'reselect';
import sagasManager from 'lib/sagasManager';
import { all, call, put, takeEvery } from 'redux-saga/effects';
import api from 'lib/api';

const initialState = {
  items: null,
  error: false,
  loading: true,
};

const actions = createActions(
  'FETCH_PROMOTIONS_REQUEST',
  'FETCH_PROMOTIONS_SUCCESS',
  'FETCH_PROMOTIONS_FAILURE',
);

const reducer = handleActions(
  {
    [actions.fetchPromotionsRequest]: state => ({
      ...state,
      loading: true,
      error: false,
    }),
    [actions.fetchPromotionsSuccess]: (state, { payload }) => ({
      ...state,
      items: payload,
      loading: false,
    }),
    [actions.fetchPromotionsFailure]: state => ({
      ...state,
      loading: false,
      error: true,
    }),
  },
  initialState,
);

const effects = {
  fetchPromotions: function* fetchPromotionsSaga() {
    try {
      const { data } = yield call(api.getPromotions);
      const promotions = data.reduce((result, item) => {
        result[item.key] = item;

        return result;
      }, {});

      yield put(actions.fetchPromotionsSuccess(promotions));
    } catch (error) {
      yield put(actions.fetchPromotionsFailure(error));
    }
  },
};

const getState = state => state.promotions;
const cs = cb => createSelector([getState], cb);

const selectors = {
  getPromotions: cs(s => s.items),
  getLoading: cs(s => s.loading),
};

sagasManager.addSagaToRoot(function* watcher() {
  yield all([
    takeEvery(actions.fetchPromotionsRequest, effects.fetchPromotions),
  ]);
});

export { initialState as state, reducer, actions, selectors, effects };
