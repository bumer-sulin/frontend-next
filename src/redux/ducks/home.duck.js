import { handleActions, createActions } from 'redux-actions';
import { createSelector } from 'reselect';
import sagasManager from 'lib/sagasManager';
import { all, call, put, takeEvery } from 'redux-saga/effects';
import { HYDRATE } from 'next-redux-wrapper';
import api from 'lib/api';

const initialState = {
  slides: [],
  photoSlides: [],
  sections: [],
  error: false,
  loading: true,
};

const actions = createActions(
  'FETCH_SLIDES_REQUEST',
  'FETCH_SLIDES_SUCCESS',
  'FETCH_SLIDES_FAILURE',

  'FETCH_PHOTO_SLIDES_REQUEST',
  'FETCH_PHOTO_SLIDES_SUCCESS',
  'FETCH_PHOTO_SLIDES_FAILURE',

  'FETCH_MAIN_SECTIONS_REQUEST',
  'FETCH_MAIN_SECTIONS_SUCCESS',
  'FETCH_MAIN_SECTIONS_FAILURE',
);

const reducer = handleActions(
  {
    [actions.fetchSlidesRequest]: state => ({
      ...state,
      loading: true,
      error: false,
    }),
    [actions.fetchSlidesSuccess]: (state, { payload }) => ({
      ...state,
      slides: payload,
      loading: false,
    }),
    [actions.fetchSlidesFailure]: state => ({
      ...state,
      loading: false,
      error: true,
    }),

    [actions.fetchPhotoSlidesRequest]: state => ({
      ...state,
      loading: true,
      error: false,
    }),
    [actions.fetchPhotoSlidesSuccess]: (state, { payload }) => ({
      ...state,
      photoSlides: payload,
      loading: false,
    }),
    [actions.fetchPhotoSlidesFailure]: state => ({
      ...state,
      loading: false,
      error: true,
    }),

    [actions.fetchMainSectionsRequest]: state => ({
      ...state,
      loading: true,
      error: false,
    }),
    [actions.fetchMainSectionsSuccess]: (state, { payload }) => ({
      ...state,
      sections: payload,
      loading: false,
    }),
    [actions.fetchMainSectionsFailure]: state => ({
      ...state,
      loading: false,
      error: true,
    }),
  },
  initialState,
);

const effects = {
  fetchSlides: function* fetchSlidesSaga() {
    try {
      const { data } = yield call(api.getSlidesArticles);
      yield put(actions.fetchSlidesSuccess(data));
    } catch (error) {
      yield put(actions.fetchSlidesFailure(error));
    }
  },

  fetchPhotoSlides: function* fetchPhotoSlidesSaga() {
    try {
      const { data } = yield call(api.getPhotoSlides);
      const updateData = data.map(e => ({
        ...e,
        category: {
          title: e.author,
        },
      }));

      yield put(actions.fetchPhotoSlidesSuccess(updateData));
    } catch (error) {
      yield put(actions.fetchPhotoSlidesFailure(error));
    }
  },

  fetchMainSections: function* fetchMainSectionsSaga() {
    try {
      const { data } = yield call(api.getMainSections);
      yield put(actions.fetchMainSectionsSuccess(data));
    } catch (error) {
      yield put(actions.fetchMainSectionsFailure(error));
    }
  },
};

const getState = state => state.home;
const cs = cb => createSelector([getState], cb);

const selectors = {
  getSlides: cs(s => s.slides),
  getPhotoSlides: cs(s => s.photoSlides),
  getSections: cs(s => s.sections),
  getErrors: cs(s => s.error),
  getLoading: cs(s => s.loading),
};

sagasManager.addSagaToRoot(function* watcher() {
  yield all([
    takeEvery(actions.fetchSlidesRequest, effects.fetchSlides),
    takeEvery(actions.fetchPhotoSlidesRequest, effects.fetchPhotoSlides),
    takeEvery(actions.fetchMainSectionsRequest, effects.fetchMainSections),
  ]);
});

export { initialState as state, reducer, actions, selectors, effects };
