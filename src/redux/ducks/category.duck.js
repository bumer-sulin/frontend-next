import { handleActions, createActions } from 'redux-actions';
import { createSelector } from 'reselect';
import sagasManager from 'lib/sagasManager';
import { all, call, put, takeEvery } from 'redux-saga/effects';
import api from 'lib/api';

const initialState = {
  items: [],
  pagination: null,
  error: false,
  loading: true,
};

const actions = createActions(
  'FETCH_DETAILS_CATEGORY_REQUEST',
  'FETCH_DETAILS_CATEGORY_SUCCESS',
  'FETCH_DETAILS_CATEGORY_FAILURE',
);

const reducer = handleActions(
  {
    [actions.fetchDetailsCategoryRequest]: state => ({
      ...state,
      loading: true,
      error: false,
    }),
    [actions.fetchDetailsCategorySuccess]: (state, { payload }) => ({
      ...state,
      items: payload.data,
      pagination: payload.pagination,
      loading: false,
    }),
    [actions.fetchDetailsCategoryFailure]: state => ({
      ...state,
      loading: false,
      error: true,
    }),
  },
  initialState,
);

const effects = {
  fetchDetailsCategory: function* fetchDetailsCategorySaga({ payload }) {
    try {
      if (payload) {
        const data = yield call(api.getCategory, payload);
        yield put(actions.fetchDetailsCategorySuccess(data));
      } else {
        const data = yield call(api.getArticles);
        yield put(actions.fetchDetailsCategorySuccess(data));
      }
    } catch (error) {
      yield put(actions.fetchDetailsCategoryFailure(error));
    }
  },
};

const getState = state => state.detailsCategory;
const cs = cb => createSelector([getState], cb);

const selectors = {
  getItems: cs(s => s.items),
  getPagination: cs(s => s.pagination),
  getLoading: cs(s => s.loading),
};

sagasManager.addSagaToRoot(function* watcher() {
  yield all([
    takeEvery(
      actions.fetchDetailsCategoryRequest,
      effects.fetchDetailsCategory,
    ),
  ]);
});

export { initialState as state, reducer, actions, selectors, effects };
