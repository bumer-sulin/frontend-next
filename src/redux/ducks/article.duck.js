import { handleActions, createActions } from 'redux-actions';
import { createSelector } from 'reselect';
import sagasManager from 'lib/sagasManager';
import { all, call, put, takeEvery, select } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import api from 'lib/api';

const initialState = {
  article: null,
  comments: [],
  error: false,
  loading: false,
  loadingComment: false,
};

const actions = createActions(
  'FETCH_DETAIL_ARTICLE_REQUEST',
  'FETCH_DETAIL_ARTICLE_SUCCESS',
  'FETCH_DETAIL_ARTICLE_FAILURE',

  'FETCH_DETAIL_COMMENT_REQUEST',
  'FETCH_DETAIL_COMMENT_SUCCESS',
  'FETCH_DETAIL_COMMENT_FAILURE',

  'LIKE_DETAIL_COMMENT_REQUEST',
  'LIKE_DETAIL_COMMENT_SUCCESS',
  'LIKE_DETAIL_COMMENT_FAILURE',

  'DISLIKE_DETAIL_COMMENT_REQUEST',
  'DISLIKE_DETAIL_COMMENT_SUCCESS',
  'DISLIKE_DETAIL_COMMENT_FAILURE',

  'POST_DETAIL_COMMENT_REQUEST',
  'POST_DETAIL_COMMENT_SUCCESS',
  'POST_DETAIL_COMMENT_FAILURE',
);

const reducer = handleActions(
  {
    [actions.fetchDetailArticleRequest]: state => ({
      ...state,
      loading: true,
      error: false,
    }),
    [actions.fetchDetailArticleSuccess]: (state, { payload }) => ({
      ...state,
      article: payload,
      loading: false,
    }),
    [actions.fetchDetailArticleFailure]: state => ({
      ...state,
      loading: false,
      error: true,
    }),

    [actions.fetchDetailCommentRequest]: state => ({
      ...state,
      loadingComment: true,
      error: false,
    }),
    [actions.fetchDetailCommentSuccess]: (state, { payload }) => ({
      ...state,
      comments: payload,
      loadingComment: false,
    }),
    [actions.fetchDetailCommentFailure]: state => ({
      ...state,
      loadingComment: false,
      error: true,
    }),

    [actions.likeDetailCommentRequest]: state => ({
      ...state,
      loadingComment: true,
      error: false,
    }),
    [actions.likeDetailCommentSuccess]: (state, { payload }) => ({
      ...state,
      loadingComment: false,
    }),
    [actions.likeDetailCommentFailure]: state => ({
      ...state,
      loadingComment: false,
      error: true,
    }),

    [actions.dislikeDetailCommentRequest]: state => ({
      ...state,
      loadingComment: true,
      error: false,
    }),
    [actions.dislikeDetailCommentSuccess]: (state, { payload }) => ({
      ...state,
      loadingComment: false,
    }),
    [actions.dislikeDetailCommentFailure]: state => ({
      ...state,
      loadingComment: false,
      error: true,
    }),

    [actions.postDetailCommentRequest]: state => ({
      ...state,
      loadingComment: true,
      error: false,
    }),
    [actions.postDetailCommentSuccess]: state => ({
      ...state,
      loadingComment: false,
    }),
    [actions.postDetailCommentFailure]: state => ({
      ...state,
      loadingComment: false,
      error: true,
    }),
  },
  initialState,
);

const effects = {
  fetchDetailArticle: function* fetchDetailArticleSaga({ payload }) {
    try {
      const data = yield call(api.getArticle, payload);

      yield put(actions.fetchDetailArticleSuccess(data));
    } catch (error) {
      yield put(actions.fetchDetailArticleFailure(error));
    }
  },

  fetchDetailComment: function* fetchDetailCommentSaga({ payload }) {
    try {
      const hash = yield select(state => state.auth.hash);
      const { data } = yield call(api.getComments, { id: payload, hash });

      yield put(actions.fetchDetailCommentSuccess(data));
    } catch (error) {
      yield put(actions.fetchDetailCommentFailure(error));
    }
  },

  postDetailComment: function* postDetailCommentSaga({ payload }) {
    try {
      const hash = yield select(state => state.auth.hash);
      const article_id = yield select(state => state.detailArticle.article.id);
      const data = yield call(api.postComment, {
        ...payload,
        id: article_id,
        hash,
      });

      yield put(actions.postDetailCommentSuccess());
      yield put(actions.fetchDetailCommentRequest(article_id));
      toast.success(`${data.message} 🚀`);
    } catch (error) {
      const { data } = error;
      yield put(actions.postDetailCommentFailure(error));
      toast.error(`${data.message}`);
    }
  },

  likeDetailComment: function* likeDetailCommentSaga({ payload }) {
    try {
      const hash = yield select(state => state.auth.hash);
      const params = {
        ...payload,
        hash,
      };

      const { data } = yield call(api.postLike, params);

      yield put(actions.likeDetailCommentSuccess(data));
      yield put(actions.fetchDetailCommentRequest(payload.article_id));
    } catch (error) {
      const { data } = error;
      toast.error(`${data.message} 😎`);
      yield put(actions.likeDetailCommentFailure(error));
    }
  },

  dislikeDetailComment: function* dislikeDetailCommentSaga({ payload }) {
    try {
      const hash = yield select(state => state.auth.hash);
      const params = {
        ...payload,
        hash,
      };

      const { data } = yield call(api.postDislike, params);

      yield put(actions.dislikeDetailCommentSuccess(data));
      yield put(actions.fetchDetailCommentRequest(payload.article_id));
    } catch (error) {
      const { data } = error;
      toast.error(`${data.message} 😎`);
      yield put(actions.dislikeDetailCommentFailure(error));
    }
  },
};

const getState = state => state.detailArticle;
const cs = cb => createSelector([getState], cb);

const selectors = {
  getArticle: cs(s => s.article),
  getLoading: cs(s => s.loading),
  getComments: cs(s => s.comments),
  // getCommentLoading
};

sagasManager.addSagaToRoot(function* watcher() {
  yield all([
    takeEvery(actions.fetchDetailArticleRequest, effects.fetchDetailArticle),
    takeEvery(actions.fetchDetailCommentRequest, effects.fetchDetailComment),
    takeEvery(actions.postDetailCommentRequest, effects.postDetailComment),
    takeEvery(actions.likeDetailCommentRequest, effects.likeDetailComment),
    takeEvery(
      actions.dislikeDetailCommentRequest,
      effects.dislikeDetailComment,
    ),
  ]);
});

export { initialState as state, reducer, actions, selectors, effects };
