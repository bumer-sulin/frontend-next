import { handleActions, createActions } from 'redux-actions';
import { createSelector } from 'reselect';
import sagasManager from 'lib/sagasManager';
import { toast } from 'react-toastify';
import { all, call, put, takeEvery } from 'redux-saga/effects';
import api from 'lib/api';
import { actions as actionsModal } from './modals.duck';

const initialState = {
  categories: [],
  settings: {},
  populars: [],
  tags: [],
  socials: [],
  subscribe: null,
  error: false,
  loading: true,
};

const actions = createActions(
  'FETCH_CATEGORIES_REQUEST',
  'FETCH_CATEGORIES_SUCCESS',
  'FETCH_CATEGORIES_FAILURE',

  'FETCH_SETTINGS_REQUEST',
  'FETCH_SETTINGS_SUCCESS',
  'FETCH_SETTINGS_FAILURE',

  'FETCH_POPULARS_REQUEST',
  'FETCH_POPULARS_SUCCESS',
  'FETCH_POPULARS_FAILURE',

  'FETCH_TAGS_REQUEST',
  'FETCH_TAGS_SUCCESS',
  'FETCH_TAGS_FAILURE',

  'FETCH_SOCIALS_REQUEST',
  'FETCH_SOCIALS_SUCCESS',
  'FETCH_SOCIALS_FAILURE',

  'SUBSCRIBE_REQUEST',
  'SUBSCRIBE_SUCCESS',
  'SUBSCRIBE_FAILURE',
);

const reducer = handleActions(
  {
    [actions.fetchCategoriesRequest]: state => ({
      ...state,
      loading: true,
      error: false,
    }),
    [actions.fetchCategoriesSuccess]: (state, { payload }) => ({
      ...state,
      categories: payload,
      loading: false,
    }),
    [actions.fetchCategoriesFailure]: state => ({
      ...state,
      loading: false,
      error: true,
    }),

    [actions.fetchSettingsRequest]: state => ({
      ...state,
      loading: true,
      error: false,
    }),
    [actions.fetchSettingsSuccess]: (state, { payload }) => ({
      ...state,
      settings: payload,
      loading: false,
    }),
    [actions.fetchSettingsFailure]: state => ({
      ...state,
      loading: false,
      error: true,
    }),

    [actions.fetchPopularsRequest]: state => ({
      ...state,
      loading: true,
      error: false,
    }),
    [actions.fetchPopularsSuccess]: (state, { payload }) => ({
      ...state,
      populars: payload,
      loading: false,
    }),
    [actions.fetchPopularsFailure]: state => ({
      ...state,
      loading: false,
      error: true,
    }),

    [actions.fetchTagsRequest]: state => ({
      ...state,
      loading: true,
      error: false,
    }),
    [actions.fetchTagsSuccess]: (state, { payload }) => ({
      ...state,
      tags: payload,
      loading: false,
    }),
    [actions.fetchTagsFailure]: state => ({
      ...state,
      loading: false,
      error: true,
    }),

    [actions.fetchSocialsRequest]: state => ({
      ...state,
      loading: true,
      error: false,
    }),
    [actions.fetchSocialsSuccess]: (state, { payload }) => ({
      ...state,
      socials: payload,
      loading: false,
    }),
    [actions.fetchSocialsFailure]: state => ({
      ...state,
      loading: false,
      error: true,
    }),

    [actions.subscribeRequest]: state => ({
      ...state,
    }),
    [actions.subscribeSuccess]: (state, { payload }) => ({
      ...state,
      subscribe: payload,
    }),
    [actions.subscribeFailure]: (state, { payload }) => ({
      ...state,
      subscribe: payload,
    }),
  },
  initialState,
);

const effects = {
  fetchCategories: function* fetchCategoriesSaga() {
    try {
      const { data } = yield call(api.getCategories);

      yield put(actions.fetchCategoriesSuccess(data));
    } catch (error) {
      yield put(actions.fetchCategoriesFailure(error));
    }
  },

  fetchSettings: function* fetchSettingsSaga() {
    try {
      const { data } = yield call(api.getSettings);
      const settings = data.reduce((result, item) => {
        const itemKey = item.key.split('.');
        const updatedKeys = () => {
          const key = itemKey[1].charAt(0).toUpperCase() + itemKey[1].substr(1);

          return itemKey[0] + key;
        };

        result[updatedKeys()] = item;

        return result;
      }, {});

      yield put(actions.fetchSettingsSuccess(settings));
    } catch (error) {
      yield put(actions.fetchSettingsFailure(error));
    }
  },

  fetchPopulars: function* fetchPopularsSaga() {
    try {
      const { data } = yield call(api.getPopularArticles);
      yield put(actions.fetchPopularsSuccess(data));
    } catch (error) {
      yield put(actions.fetchPopularsFailure(error));
    }
  },

  fetchTags: function* fetchTagsSaga() {
    try {
      const { data } = yield call(api.getTags);
      yield put(actions.fetchTagsSuccess(data));
    } catch (error) {
      yield put(actions.fetchTagsFailure(error));
    }
  },

  fetchSocials: function* fetchSocialsSaga() {
    try {
      const { data } = yield call(api.getSocials);
      yield put(actions.fetchSocialsSuccess(data));
    } catch (error) {
      yield put(actions.fetchSocialsFailure(error));
    }
  },
  subscribe: function* subscribeSaga({ payload }) {
    try {
      yield call(api.postSubscribe, payload);
      toast.success('Спасибо за подписку');
    } catch (error) {
      toast.error(error.data.message);
    }
  },
};

const getState = state => state.main;
const cs = cb => createSelector([getState], cb);

const selectors = {
  getCategories: cs(s => s.categories),
  getSettings: cs(s => s.settings),
  getPopulars: cs(s => s.populars),
  getSocials: cs(s => s.socials),
  getTags: cs(s => s.tags),
  getSubscribe: cs(s => s.subscribe),
  getErrors: cs(s => s.error),
  getLoading: cs(s => s.loading),
};

sagasManager.addSagaToRoot(function* watcher() {
  yield all([
    takeEvery(actions.fetchCategoriesRequest, effects.fetchCategories),
    takeEvery(actions.fetchSettingsRequest, effects.fetchSettings),
    takeEvery(actions.fetchPopularsRequest, effects.fetchPopulars),
    takeEvery(actions.fetchTagsRequest, effects.fetchTags),
    takeEvery(actions.fetchSocialsRequest, effects.fetchSocials),
    takeEvery(actions.subscribeRequest, effects.subscribe),
  ]);
});

export { initialState as state, reducer, actions, selectors, effects };
