import React, { useEffect } from 'react';
import { ThemeProvider } from 'styled-components';
import { colors, GlobalStyle } from 'styles';
import { wrapper } from 'redux/store';
import { ToastContainer } from 'react-toastify';
import Head from 'next/head';
import Wrap from 'views/Wrap';
import Modal from 'components/Modals';
import 'react-toastify/dist/ReactToastify.css';
import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';
import 'swiper/components/scrollbar/scrollbar.scss';
import 'react-datepicker/dist/react-datepicker.css';

function App({ Component, pageProps }) {
  return (
    <ThemeProvider theme={{ ...colors }}>
      <React.Fragment>
        <Wrap>
          <Head>
            <link
              href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700"
              rel="stylesheet"
            />
          </Head>
          <Component {...pageProps} />
          <Modal selector="#modal" />
        </Wrap>
        <GlobalStyle />
        <ToastContainer autoClose={3000} toastClassName="toast-custom" />
      </React.Fragment>
    </ThemeProvider>
  );
}

export default wrapper.withRedux(App);
