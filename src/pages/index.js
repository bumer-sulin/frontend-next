import { Container, media } from 'styles';
import { connect } from 'react-redux';
import { selectors, actions } from 'redux/ducks/main.duck';
import {
  selectors as selectorsUser,
  actions as actionsUser,
} from 'redux/ducks/user.duck';
import {
  selectors as selectoresPromotions,
  actions as actionsPromotions,
} from 'redux/ducks/promotions.duck';
import { actions as actionsModals } from 'redux/ducks/modals.duck';
import styled from 'styled-components';
import Home from '../views/Home';

const Index = props => {
  return <Home settings={props.settings} />;
};

export default connect(
  state => ({
    categories: selectors.getCategories(state),
    populars: selectors.getPopulars(state),
    loading: selectors.getLoading(state),
    tags: selectors.getTags(state),
    socials: selectors.getSocials(state),
    settings: selectors.getSettings(state),
    promotions: selectoresPromotions.getPromotions(state),
    loadingPromotions: selectoresPromotions.getLoading(state),
    profile: selectorsUser.getProfile(state),
  }),
  { ...actions, ...actionsPromotions, ...actionsUser, ...actionsModals },
)(Index);

const Content = styled.div``;

const MainContent = styled.div`
  ${Container}
  padding-top: 40px;
  margin-bottom: 60px;
  width: 100%;
`;

const MainContainer = styled.div`
  display: grid;
  width: 100%;
  margin: 0 auto;
  grid-template-columns: 780px auto;
  grid-gap: 0 30px;
  ${media.desktop`
    grid-template-columns: 100%;
  `}
`;
