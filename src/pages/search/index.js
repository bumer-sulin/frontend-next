import styled from 'styled-components';
import { Container, media } from 'styles';
import Categories from 'views/Categories';

const Search = props => {
  return (
    <MainContainer>
      <Categories
        title="Categories Page"
        linkTo="/other"
        NavigateTo="Other Page"
        // settings={settings}
      />
    </MainContainer>
  );
};

export default Search;

const Content = styled.div``;

const MainContent = styled.div`
  ${Container}
  padding-top: 40px;
  margin-bottom: 60px;
  width: 100%;
`;

const MainContainer = styled.div`
  display: grid;
  width: 100%;
  margin: 0 auto;
  grid-template-columns: 780px auto;
  grid-gap: 0 30px;
  ${media.desktop`
    grid-template-columns: 100%;
  `}
`;
