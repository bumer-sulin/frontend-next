import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { useRouter } from 'next/router';
import styled from 'styled-components';
import { Container, media } from 'styles';
import { LoadingDefault } from 'components/UI/Loading';
import Detail from 'views/Detail';

const Post = props => {
  const [isLoaded, setIsLoaded] = useState(false);
  const { query } = useRouter();

  useEffect(() => {
    if (query.slug) {
      setIsLoaded(true);
    }

    return () => {};
  }, [query]);

  return isLoaded ? <Detail /> : <LoadingDefault />;
};

export default Post;

const Content = styled.div``;

const MainContent = styled.div`
  ${Container}
  padding-top: 40px;
  margin-bottom: 60px;
  width: 100%;
`;

const MainContainer = styled.div`
  display: grid;
  width: 100%;
  margin: 0 auto;
  grid-template-columns: 780px auto;
  grid-gap: 0 30px;
  ${media.desktop`
    grid-template-columns: 100%;
  `}
`;
