export default props => {
  if (!props) {
    return '';
  }

  const { search, category_id, tags, date, page } = props;
  const params = new URLSearchParams('');

  if (search) {
    params.append('search', search);
  }

  if (category_id) {
    params.append('category_id', category_id);
  }

  if (tags) {
    tags.forEach(e => {
      params.append('tags[]', e);
    });
  }

  if (page) {
    params.append('page', page);
  }

  if (date) {
    params.append('date', date);
  }

  return params.toString();
};
