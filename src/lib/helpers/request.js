import axios from 'axios';

export default async (method, url, options = {}, config = null) => {
  const instance = axios.create({
    // baseURL: 'https://bumer-media.ru/api',
    baseURL: process.env.apiKey,
    headers: {},
  });
  try {
    const response = await instance[method](url, options, config);

    return response.data;
  } catch (err) {
    const error = await err.response;

    throw error;
  }
};
