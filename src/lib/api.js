import request from './helpers/request';
import searchCategory from './helpers/searchCategory';

// const createParams = (val) => {
//   const keys = Object.keys(val);

//   return keys.map(e => )
// }

export default {
  getCategories: () => request('get', '/categories'),
  getPopularArticles: () => request('get', '/articles?type=popular'),
  getSlidesArticles: () => request('get', '/articles?type=main'),
  getPhotoSlides: () => request('get', '/articles?type=gallery'),
  getSettings: () => request('get', '/settings'),
  getTags: () => request('get', '/tags'),
  getSocials: () => request('get', '/socials'),
  getMainSections: () => request('get', '/sections'),
  getCategory: params => request('get', `/articles?${searchCategory(params)}`),
  getArticles: () => request('get', '/articles'),
  getArticle: id => request('get', `/article/${id}`),
  getPromotions: () => request('get', '/promotions'),
  postSubscribe: email => request('post', `/subscribe?email=${email}`),
  authLogin: params => request('post', '/login', params),
  authSingup: params => request('post', '/register', params),
  authGetPass: email => request('post', `password/email?email=${email}`),
  resetPass: ({ email, reset_token, password, password_confirmation }) =>
    request(
      'post',
      `password/reset?email=${email}&password=${password}&password_confirmation=${password_confirmation}&token=${reset_token}`,
    ),
  getProfile: hash => request('get', `/user${hash ? `?hash=${hash}` : ''}`),
  updateProfile: ({ hash, id, ...params }) =>
    request('post', `user/${id}/update?hash=${hash}`, params),
  getComments: ({ id, hash }) =>
    request('get', `/article/${id}/comments?${hash ? `hash=${hash}` : ''} `),
  postLike: ({ article_id, comment_id, hash }) =>
    request(
      'post',
      `article/${article_id}/comment/${comment_id}/like?hash=${hash}`,
    ),
  postDislike: ({ article_id, comment_id, hash }) =>
    request(
      'post',
      `article/${article_id}/comment/${comment_id}/dislike?hash=${hash}`,
    ),
  postComment: ({ id, ...params }) =>
    request('post', `article/${id}/comment`, params),
};
