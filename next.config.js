const webpack = require('webpack');
const withImages = require('next-images');
const withFonts = require('next-fonts');

module.exports = withFonts(
  withImages({
    env: {
      apiKey: 'https://bumer-media.ru/api',
    },
    webpack: (config, { dev }) => {
      if (dev) {
        config.module.rules.push({
          test: /\.js$/,
          loader: 'eslint-loader',
          exclude: ['/node_modules/', '/.next/', '/out/'],
          enforce: 'pre',
          options: {
            emitWarning: true,
            fix: true,
          },
        });
      }
      return config;
    },
  }),
);
